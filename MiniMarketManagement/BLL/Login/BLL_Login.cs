﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Login;
using MiniMarketManagement.BLL.Cashier;
using MiniMarketManagement.GUI;
using System.Windows.Forms;
using MiniMarketManagement.GUI.Administrator;
using MiniMarketManagement.GUI.Cashier;
using MiniMarketManagement.GUI.Storekeeper;
using MiniMarketManagement.DAL.Cashier;
using MiniMarketManagement.BLL.Storekeeper;

namespace MiniMarketManagement.BLL.Cashier
{
    public class BLL_Login
    {
        DAL_Login login = new DAL_Login();
        BLL_Payment bll_payment = new BLL_Payment();
        BLL_ImportProduct BLL_ImportProduct = new BLL_ImportProduct();
        public void Login(string username, string password, frm_Login frm_login)
        {
            if (username != "" && password != "")
            {
                bool found = false;
                var accounts = login.GetAccountList();
                foreach (var account in accounts)
                {
                    if (account.Username.ToString().Trim().Equals(username) && account.Password.ToString().Trim().Equals(password))
                    {
                        found = true;
                        OpenForm(account.Employee.PositionID.ToString().Trim());
                        frm_login.Hide();
                    }
                }
                if (!found)
                {
                    MessageBox.Show("Sai Username hoặc Password");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin!");
            }
        }
        private void OpenForm(string positionID)
        {
            if (positionID == "CV01")
            {
                frm_Admin frm_admin = new frm_Admin();
                frm_admin.Show();
            }
            if (positionID == "CV02")
            {
                frm_WarehouseManagement frm_warehouseManagement = new frm_WarehouseManagement();
                frm_warehouseManagement.Show();
            }
            if (positionID == "CV03")
            {
                frm_Cashier frm_cashier = new frm_Cashier();
                frm_cashier.Show();
            }
        }
        public void ShowPassword(Guna2ToggleSwitch cb_ShowPassword, Guna2TextBox txt_Password)
        {
            if (cb_ShowPassword.Checked)
            {
                txt_Password.PasswordChar = '\0';
            }
            else
            {
                txt_Password.PasswordChar = '●';
            }
        }
        public void saveUser(string txt_Username)
        {
            bll_payment.saveUser(txt_Username);
            BLL_ImportProduct.saveUser(txt_Username);
        }
    }
}
