﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Login;
using MiniMarketManagement.DAL.Storekeeper;
namespace MiniMarketManagement.BLL.Storekeeper
{
    internal class BLL_ReturnProduct
    {
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void Filldgv_ListImportProductCard(Guna2DataGridView dgv_ListImportProductCard)
        {
            DAL_ReturnProduct_Storekeeper dAL_ReturnProduct_Storekeeper = new DAL_ReturnProduct_Storekeeper();
            var list = dAL_ReturnProduct_Storekeeper.GetListDetailImportProductCards();
            dgv_ListImportProductCard.Rows.Clear();
            foreach (var card in list)
            {
                int index = dgv_ListImportProductCard.Rows.Add();
                dgv_ListImportProductCard.Rows[index].Cells["ImportProductID"].Value = card.ImportProductID;
                dgv_ListImportProductCard.Rows[index].Cells["ProductID"].Value = card.ProductID;
                dgv_ListImportProductCard.Rows[index].Cells["Product__Name"].Value = card.Product.ProductName;
                dgv_ListImportProductCard.Rows[index].Cells["BuyPrice"].Value = card.Product.BuyPrice;
                dgv_ListImportProductCard.Rows[index].Cells["ProductQuantity"].Value = card.Quantity;
                dgv_ListImportProductCard.Rows[index].Cells["SupplierName"].Value = card.ImportProductCard.Supplier.SupplierName;
                // Định dạng ngày/tháng/năm
                if (card.ImportProductCard.ImportProductDate != null)
                {
                    dgv_ListImportProductCard.Rows[index].Cells["ImportProductDate"].Value = card.ImportProductCard.ImportProductDate.ToString("dd/MM/yyyy");
                }
            }
        }
        public void SearchImportProductCard(string txt, Guna2DataGridView dgv_ListProduct, string fieldIPID, string fieldPID, string fieldName)
        {
            List<int> rowIndexesToShow = new List<int>();
            for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
            {
                dgv_ListProduct.Rows[i].Visible = false;
                if ((string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells[fieldIPID].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells[fieldPID].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells[fieldName].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower()))
                   )
                {
                    rowIndexesToShow.Add(i);
                }
            }
            foreach (int rowIndex in rowIndexesToShow)
            {
                dgv_ListProduct.Rows[rowIndex].Visible = true;
            }
        }
        public void FillAllTxtBoxExceptTxtSearch(int row, Guna2DataGridView dgv_ListProduct, out Image image, out string ImportProductID,
                                                 out string ProductID, out string ProductName, out string totalPrice, out string SupplierName,
                                                 out string IPDate, string fieldIPID, string fieldPID, string fieldPName, string fieldPrice,
                                                 string fieldQuantity, string fieldSupplierName, string fieldIPDate)
        {
            ImportProductID = dgv_ListProduct.Rows[row].Cells[fieldIPID].Value.ToString();
            ProductID = dgv_ListProduct.Rows[row].Cells[fieldPID].Value.ToString();
            ProductName = dgv_ListProduct.Rows[row].Cells[fieldPName].Value.ToString();
            totalPrice = (double.Parse(dgv_ListProduct.Rows[row].Cells[fieldPrice].Value.ToString()) *
                          double.Parse(dgv_ListProduct.Rows[row].Cells[fieldQuantity].Value.ToString())).ToString();
            SupplierName = dgv_ListProduct.Rows[row].Cells[fieldSupplierName].Value.ToString();
            IPDate = dgv_ListProduct.Rows[row].Cells[fieldIPDate].Value.ToString();
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
            var ImPath = product.ProductImage;
            string imagePath;
            if (string.IsNullOrEmpty(ImPath))
            {
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
            }
            else
            {
                // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
            }
            image = Image.FromFile(imagePath);
        }
        public void ClearInputFields(out Image image, out string ImportProductID,
                                     out string ProductID, out string ProductName, out string totalPrice, out string SupplierName, out string IPDate)
        {
            ImportProductID = "";
            ProductID = "";
            ProductName = "";
            totalPrice = "";
            SupplierName = "";
            IPDate = "";
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string imagepath = Path.Combine(projectDirectory, EmptyImPath);
            image = Image.FromFile(imagepath);
        }
        public void DisplayNearestIPC(Guna2DataGridView dgv_ListProduct, string IPIDField)
        {
            List<int> rowIndexesToShow = new List<int>();
            DAL_ReturnProduct_Storekeeper dAL_ReturnProduct_Storekeeper = new DAL_ReturnProduct_Storekeeper();
            DateTime currentDate = DateTime.Now;
            for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
            {
                var IPC = dAL_ReturnProduct_Storekeeper.GetImportProductCardByImportProductID(dgv_ListProduct.Rows[i].Cells[IPIDField].Value.ToString().Trim());
                dgv_ListProduct.Rows[i].Visible = false;
                if (currentDate - IPC.ImportProductDate <= TimeSpan.FromDays(1) && dgv_ListProduct.Rows[i].Cells[IPIDField].Value.ToString().Trim() == IPC.ImportProductID.Trim())
                {
                    rowIndexesToShow.Add(i);
                }
            }
            foreach (int rowIndex in rowIndexesToShow)
            {
                dgv_ListProduct.Rows[rowIndex].Visible = true;
            }
        }
        public int GetRowIndex(Guna2DataGridView dgv_ListSelectedProduct, string IPCID, string ProductID, string fieldID, string fieldIPCID)
        {
            for (int i = 0; i < dgv_ListSelectedProduct.Rows.Count; i++)
            {
                if (dgv_ListSelectedProduct.Rows[i].Cells[fieldID].Value.ToString().Trim() == ProductID.Trim() && dgv_ListSelectedProduct.Rows[i].Cells[fieldIPCID].Value.ToString().Trim() == IPCID.Trim())
                {
                    return i;
                }
            }
            return -1;
        }
        public int importDIPCToSelectedDIPCByIPCID(Guna2DataGridView dgv_ListSelectedImportProductCards, string IPCID, string PID)
        {
            if (string.IsNullOrEmpty(IPCID) || string.IsNullOrEmpty(PID))
            {
                MessageBox.Show("Vui lòng chọn ít nhất một sản phẩm trong danh sách");
                return 0;
            }
            DAL_ReturnProduct_Storekeeper dAL_ReturnProduct_Storekeeper = new DAL_ReturnProduct_Storekeeper();
            var IPC = dAL_ReturnProduct_Storekeeper.GetDetailImportProductCardByIPCIDAndPID(IPCID, PID);
            try
            {
                if (IPC == null)
                {
                    MessageBox.Show("Không có mã sản phẩm bạn bạn chọn trong danh sách!!");
                    return 0;
                }
                else
                {
                    int selectedRow = GetRowIndex(dgv_ListSelectedImportProductCards, IPC.ImportProductID, IPC.ProductID, "Product_ID", "Import_Product_ID");
                    if (selectedRow == -1)
                    {
                        int index = dgv_ListSelectedImportProductCards.Rows.Add();
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Import_Product_ID"].Value = IPC.ImportProductID;
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Product_ID"].Value = IPC.ProductID;
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Product_Name"].Value = IPC.Product.ProductName;
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Buy_Price"].Value = IPC.Product.BuyPrice;
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Product_Quantity"].Value = IPC.Quantity;
                        dgv_ListSelectedImportProductCards.Rows[index].Cells["Supplier_Name"].Value = IPC.ImportProductCard.Supplier.SupplierName;
                        // Định dạng ngày/tháng/năm
                        if (IPC.ImportProductCard.ImportProductDate != null)
                        {
                            dgv_ListSelectedImportProductCards.Rows[index].Cells["Import_Product_Date"].Value = IPC.ImportProductCard.ImportProductDate.ToString("dd/MM/yyyy");
                        }
                        return 1;
                    }
                    else
                    {
                        MessageBox.Show("Sản phẩm bạn chọn đã được chọn!!");
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi khi thêm đó là: " + ex.InnerException.Message);
                return 0;
            }
        }
        public double sumBuyPriceOfSelectedIPCList(Guna2DataGridView dgv_ListSelectedProduct, string BuyPriceField, string QuantityField)
        {
            double sum = 0;
            for (int i = 0; i < dgv_ListSelectedProduct.Rows.Count; i++)
            {
                sum += (double.Parse(dgv_ListSelectedProduct.Rows[i].Cells[BuyPriceField].Value.ToString()) * double.Parse(dgv_ListSelectedProduct.Rows[i].Cells[QuantityField].Value.ToString()));
            }
            return sum;
        }
        public int DeleteDIPC(Guna2DataGridView dgv_ListSelectedImportProductCards, string IPCID, string PID)
        {
            if (string.IsNullOrEmpty(IPCID) || string.IsNullOrEmpty(PID))
            {
                MessageBox.Show("Vui lòng chọn ít nhất một sản phẩm trong danh sách");
                return 0;
            }
            DAL_ReturnProduct_Storekeeper dAL_ReturnProduct_Storekeeper = new DAL_ReturnProduct_Storekeeper();
            var IPC = dAL_ReturnProduct_Storekeeper.GetDetailImportProductCardByIPCIDAndPID(IPCID, PID);
            try
            {
                int selectedRow = GetRowIndex(dgv_ListSelectedImportProductCards, IPC.ImportProductID, IPC.ProductID, "Product_ID", "Import_Product_ID");
                if (selectedRow == -1)
                {
                    MessageBox.Show("Không có mặt hàng bạn muốn xóa!!");
                    return 0;
                }
                else
                {
                    var result = MessageBox.Show("Bạn có chắc muốn xóa mặt hàng này khỏi danh sách bạn chọn!!\nNếu nhấn Yes sẽ xóa nó!!", "Xóa", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        dgv_ListSelectedImportProductCards.Rows.RemoveAt(selectedRow);
                        return 1;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi khi xóa đó là: " + ex.InnerException.Message);
                return 0;
            }
        }
        static string user;
        public void saveUser(string txt_Username)
        {
            user = txt_Username;

        }
        public string GetEmployeeIDByUserName()
        {
            DAL_Login dAL_Login = new DAL_Login();
            var accountsList = dAL_Login.GetAccountList();
            foreach (var account in accountsList)
            {
                if (account.Username == user)
                {
                    return account.EmployeeID;
                }
            }
            return null;
        }
        public int AddSelectedDIPCToListRPCAndDRPC(Guna2DataGridView dgv_SelectedDIPC, Guna2DataGridView dgv_ListProducts)
        {
            var result = MessageBox.Show("Bạn có chắc muốn trả hàng", "Trả hàng", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    DAL_ReturnProduct_Storekeeper dAL_ReturnProduct_Storekeeper = new DAL_ReturnProduct_Storekeeper();
                    //string EID = GetEmployeeIDByUserName();
                    string EID = "NV2";
                    List<ReturnProductsCard> listRPC = new List<ReturnProductsCard>();
                    List<DetailReturnProductCard> listDRPC = new List<DetailReturnProductCard>();
                    List<int> rowsToRemove = new List<int>(); // Danh sách các dòng cần xóa

                    int index = (dAL_ReturnProduct_Storekeeper.GetLastIndexOfReturnProductCard() + 1);
                    for (int row = 0; row < dgv_SelectedDIPC.Rows.Count; row++)
                    {
                        var DIPC = dAL_ReturnProduct_Storekeeper.GetDetailImportProductCardByIPCIDAndPID(dgv_SelectedDIPC.Rows[row].Cells["Import_Product_ID"].Value.ToString(),
                                                                                                         dgv_SelectedDIPC.Rows[row].Cells["Product_ID"].Value.ToString());
                        
                        if (!listRPC.Any(ipc => ipc.SupplierID == DIPC.ImportProductCard.SupplierID))
                        {
                            ReturnProductsCard RPC = new ReturnProductsCard();
                            RPC.ReturnProductID = "RP" + index.ToString();
                            RPC.SupplierID = DIPC.ImportProductCard.SupplierID;
                            RPC.ReturnProductDate = DateTime.Now;
                            RPC.EmployeeID = EID;
                            listRPC.Add(RPC);
                            index++;
                        }
                        else
                        {
                            foreach(var t in listRPC)
                            {
                                var DIPCT = dAL_ReturnProduct_Storekeeper.
                                            GetDetailImportProductCardByIPCIDAndPID(dgv_SelectedDIPC.Rows[row].Cells["Import_Product_ID"].Value.ToString(),
                                                                                    dgv_SelectedDIPC.Rows[row].Cells["Product_ID"].Value.ToString());
                            }
                            
                        }

                        DetailReturnProductCard DRPC = new DetailReturnProductCard();
                        DRPC.ReturnProductID = listRPC.Last().ReturnProductID;
                        DRPC.ProductID = DIPC.ProductID;
                        DRPC.Quantity = DIPC.Quantity;
                        DRPC.price = DIPC.Price;

                        // Kiểm tra xem sản phẩm đã có trong listDIPC chưa
                        if (!listDRPC.Any(d => d.ProductID == DIPC.ProductID))
                        {
                            listDRPC.Add(DRPC);
                            rowsToRemove.Add(row);
                        }

                        for (int i = row + 1; i < dgv_SelectedProduct.Rows.Count; i++)
                        {
                            var productI = dAL_ImportProduct_Storekeeper.GetProductByProductID(dgv_SelectedProduct.Rows[i].Cells["Product_ID"].Value.ToString());
                            if (productI.SupplierID.Trim() == product.SupplierID.Trim())
                            {
                                DetailImportProductCard DIPCI = new DetailImportProductCard();
                                DIPCI.ImportProductID = listIPC.Last().ImportProductID;
                                DIPCI.ProductID = productI.ProductID;
                                DIPCI.Quantity = double.Parse(dgv_SelectedProduct.Rows[i].Cells["Product_Quantity"].Value.ToString());
                                DIPCI.Price = double.Parse(dgv_SelectedProduct.Rows[i].Cells["Buy_Price"].Value.ToString());

                                // Kiểm tra xem sản phẩm đã có trong listDIPC chưa
                                if (!listDIPC.Any(d => d.ProductID == DIPCI.ProductID))
                                {
                                    listDIPC.Add(DIPCI);
                                    rowsToRemove.Add(row);
                                }
                            }
                        }
                    }
                    foreach (var list in listIPC)
                    {
                        double sum = 0;
                        foreach (var item in listDIPC)
                        {
                            if (item.ImportProductID == list.ImportProductID)
                            {
                                sum += (item.Price * item.Quantity);
                            }
                        }
                        list.PriceTotal = sum;
                    }
                    if (listIPC.Count > 0)
                    {
                        dAL_ImportProduct_Storekeeper.AddImportProductCardAndDetailImportProductCard(listIPC, listDIPC);
                        FillDGV(dgv_ListProducts);
                        MessageBox.Show("Đã thanh toán thành công");
                        rowsToRemove.Sort();
                        // Xóa các dòng đã được xác định
                        for (int i = rowsToRemove.Count - 1; i >= 0; i--)
                        {
                            dgv_SelectedProduct.Rows.RemoveAt(rowsToRemove[i]);
                        }
                        return 1;
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng thêm ít nhất 1 sản phẩm!!");
                        return 0;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi trong quá trình thanh toán đó là: " + ex.Message);

                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
    }
}
