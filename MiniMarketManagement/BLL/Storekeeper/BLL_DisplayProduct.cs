﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Storekeeper;
namespace MiniMarketManagement.BLL.Storekeeper
{
    internal class BLL_DisplayProduct
    {
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void FormDisplayProduct(Guna2DataGridView dgv_ListProduct)
        {
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var list = dAL_ImportProduct_Storekeeper.GetProductListinRepository();
            dgv_ListProduct.Rows.Clear();
            foreach (var item in list)
            {
                int index = dgv_ListProduct.Rows.Add();
                dgv_ListProduct.Rows[index].Cells["Product_ID"].Value = item.ProductID;
                dgv_ListProduct.Rows[index].Cells["Product_Name"].Value = item.Product.ProductName;
                dgv_ListProduct.Rows[index].Cells["Product_Type_Name"].Value = item.Product.ProductType.ProductTypeName;
                dgv_ListProduct.Rows[index].Cells["Buy_Price"].Value = item.Product.BuyPrice;
                dgv_ListProduct.Rows[index].Cells["Sell_Price"].Value = item.SellPrice;
                dgv_ListProduct.Rows[index].Cells["Calculation_Unit_Name"].Value = item.Product.ProductType.CalculationUnitName;
                dgv_ListProduct.Rows[index].Cells["ProductQuantity"].Value = item.ProductQuantity;
                dgv_ListProduct.Rows[index].Cells["Supplier"].Value = item.Supplier.SupplierName;
            }
            
        }

        public void SearchProduct(string txt, Guna2DataGridView dgv_ListProduct)
        {
            List<int> rowIndexesToShow = new List<int>();
            for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
            {
                dgv_ListProduct.Rows[i].Visible = false;
                if ((string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells["Product_ID"].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells["Product_Name"].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower()))
                   )
                {
                    rowIndexesToShow.Add(i);
                }
            }
            foreach (int rowIndex in rowIndexesToShow)
            {
                dgv_ListProduct.Rows[rowIndex].Visible = true;
            }
        }

        public void FillPicbox(int row, Guna2DataGridView dgv_ListProduct, out Image image, out string productID, out string productName)
        {
            productID = dgv_ListProduct.Rows[row].Cells["Product_ID"].Value.ToString();
            productName = dgv_ListProduct.Rows[row].Cells["Product_Name"].Value.ToString();
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(productID);
            var ImPath = product.ProductImage;
            string imagePath;
            if (string.IsNullOrEmpty(ImPath))
            {
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
            }
            else
            {
                // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
            }
            image = Image.FromFile(imagePath);
        }
        public void ClearInputFields(out Image picbProduct, out string txtID, out string txtName)
        {
            txtID = "";
            txtName = "";
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string imagepath = Path.Combine(projectDirectory, EmptyImPath);
            picbProduct = Image.FromFile(imagepath);
        }
    }
}
