﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniMarketManagement.DAL.Cashier;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace MiniMarketManagement.DAL.Cashier
{
    public class BLL_Payment
    {
        DAL_Payment dal_payment = new DAL_Payment();
        DAL_Login dal_login = new DAL_Login();
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void LoadDgvFindProduct(Guna2DataGridView dgvFind_Product)
        {
            dgvFind_Product.Rows.Clear();
            var listProduct = dal_payment.GetAllProduct();
            foreach (var item in listProduct)
            {
                int index = dgvFind_Product.Rows.Add();
                dgvFind_Product.Rows[index].Cells["dgvFind_ProductID"].Value = item.ProductID;
                dgvFind_Product.Rows[index].Cells["dgvFind_ProductName"].Value = item.ProductName;
                dgvFind_Product.Rows[index].Cells["dgvFind_TypeProductName"].Value = item.ProductType.ProductTypeName;
                dgvFind_Product.Rows[index].Cells["dgvFind_CalculationUnit"].Value = item.ProductType.CalculationUnitName;
                var getProductID_in_Repository = dal_payment.GetRepositorySearch(item.ProductID);
                foreach(var i in getProductID_in_Repository)
                {
                    dgvFind_Product.Rows[index].Cells["dgvFind_SellPrice"].Value = i.SellPrice;
                    dgvFind_Product.Rows[index].Cells["dgvFind_QuantityProduct"].Value = i.ProductQuantity;
                    if(i.Discount == null)
                    {
                        dgvFind_Product.Rows[index].Cells["dgvFind_ShowDiscount"].Value = 0;
                    }
                    else
                    {
                        dgvFind_Product.Rows[index].Cells["dgvFind_ShowDiscount"].Value = i.Discount;
                    }
                }



            }
        }

        public void LoadCellDgvFindProduct(object sender, DataGridViewCellEventArgs e, Guna2DataGridView dgvFind_Product,
                    Guna2TextBox txt_ProductID, Guna2TextBox txt_ProductName, out Image image)
        {

            int index = e.RowIndex;
            if(index < 0)
            {
                image = null;
                return;
            }
            txt_ProductName.Text = dgvFind_Product.Rows[index].Cells["dgvFind_ProductName"].Value.ToString();
            txt_ProductID.Text = dgvFind_Product.Rows[index].Cells["dgvFind_ProductID"].Value.ToString();

            var ImPath = dal_payment.GetProduct(txt_ProductID.Text).ProductImage;
            string imagePath;
            if (string.IsNullOrEmpty(ImPath))
            {
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
            }
            else
            {
                // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
            }
            image = Image.FromFile(imagePath);
        }

        public void btn_AddOrUpdate_DgvPayProduct(Guna2DataGridView dgvPayment_Product,
            Guna2TextBox txt_ProductID, Guna2TextBox txt_ProductName, Guna2TextBox txt_Quantity)
        {
            if (txt_ProductID.Text != "")
            {
                bool check = false;
                int t = dgvPayment_Product.Rows.Count;
                var listProduct = dal_payment.GetAllProduct();
                foreach (var item in listProduct)
                {
                    for (int i = 0; i < t; i++)
                    {
                        var checkdgv_ProductID = dgvPayment_Product.Rows[i].Cells["dgvPay_ProductID"].Value.ToString();
                        if (checkdgv_ProductID == txt_ProductID.Text)
                        {
                            if(txt_Quantity.Text == "")
                            {
                                MessageBox.Show("Vui lòng nhập số lượng!");
                                return;
                            }
                            else
                            {
                                dgvPayment_Product.Rows[i].Cells["dgvPay_Quantity"].Value = txt_Quantity.Text;
                                check = true;
                                txt_ProductID.Enabled = true;
                                txt_ProductName.Enabled = true;
                                txt_Quantity.Text = "";
                                txt_ProductID.Text = "";
                                txt_ProductName.Text = "";
                                return;
                            }
                        }
                    }
                    if (check == false)
                    {
                        if (item.ProductID == txt_ProductID.Text)
                        {
                            int.TryParse(txt_Quantity.Text, out int temp);
                            if (temp == 0)
                            {
                                MessageBox.Show("Số lượng phải là số nguyên và không được để trống!");
                                return;
                            }
                            int index = dgvPayment_Product.Rows.Add();   
                            dgvPayment_Product.Rows[index].Cells["dgvPay_ProductID"].Value = item.ProductID;
                            dgvPayment_Product.Rows[index].Cells["dgvPay_ProductName"].Value = item.ProductName;
                            dgvPayment_Product.Rows[index].Cells["dgvPay_TypeProductName"].Value = item.ProductType.ProductTypeName;
                            dgvPayment_Product.Rows[index].Cells["dgvPay_CalculationUnit"].Value = item.ProductType.CalculationUnitName;
                            var getProductID = dal_payment.GetRepositorySearch(item.ProductID);
                            foreach (var i in getProductID)
                            {
                                dgvPayment_Product.Rows[index].Cells["dgvPay_SellPrice"].Value = i.SellPrice;
                                if (i.Discount == null)
                                {
                                    dgvPayment_Product.Rows[index].Cells["dgvPay_ShowDiscount"].Value = 0;
                                }
                                else
                                {
                                    dgvPayment_Product.Rows[index].Cells["dgvPay_ShowDiscount"].Value = i.Discount;
                                }
                            }
                            dgvPayment_Product.Rows[index].Cells["dgvPay_TimeNow"].Value = DateTime.Now.ToString("yyyy-MM-dd");
                            dgvPayment_Product.Rows[index].Cells["dgvPay_Quantity"].Value = txt_Quantity.Text;
                            txt_Quantity.Text = "";
                            txt_ProductID.Text = "";
                            txt_ProductName.Text = "";
                        }
                    }

                }
            }
            else
            {
                MessageBox.Show("Vui Lòng nhập mã sản phẩm cần thêm hoặc sửa!");
            }
        }


        public void LoadCellDgvPayProduct(object sender, DataGridViewCellEventArgs e, Guna2DataGridView dgvPayment_Product,
    Guna2TextBox txt_ProductID, Guna2TextBox txt_ProductName, Guna2TextBox txt_Quantity)
        {
            int index = e.RowIndex;
            if(index < 0) 
            { 
                return; 
            }
            txt_ProductID.Text = dgvPayment_Product.Rows[index].Cells["dgvPay_ProductID"].Value.ToString();
            txt_ProductName.Text = dgvPayment_Product.Rows[index].Cells["dgvPay_ProductName"].Value.ToString();
            txt_Quantity.Text = dgvPayment_Product.Rows[index].Cells["dgvPay_Quantity"].Value.ToString();
        }

        public void SearchByProductId_Or_Name(string txt_search, Guna2DataGridView dgvFind_Product)
        {
            dgvFind_Product.Rows.Clear();
            var listProduct = dal_payment.GetProductSearch(txt_search);
            foreach (var item in listProduct)
            {
                int index = dgvFind_Product.Rows.Add();
                dgvFind_Product.Rows[index].Cells["dgvFind_ProductID"].Value = item.ProductID;
                dgvFind_Product.Rows[index].Cells["dgvFind_ProductName"].Value = item.ProductName;
                dgvFind_Product.Rows[index].Cells["dgvFind_TypeProductName"].Value = item.ProductType.ProductTypeName;
                dgvFind_Product.Rows[index].Cells["dgvFind_CalculationUnit"].Value = item.ProductType.CalculationUnitName;
                var getProductID = dal_payment.GetRepositorySearch(item.ProductID);
                foreach (var i in getProductID)
                {
                    dgvFind_Product.Rows[index].Cells["dgvFind_SellPrice"].Value = i.SellPrice;
                    dgvFind_Product.Rows[index].Cells["dgvFind_QuantityProduct"].Value = i.ProductQuantity;
                    if (i.Discount == null)
                    {
                        dgvFind_Product.Rows[index].Cells["dgvFind_ShowDiscount"].Value = 0;
                    }
                    else
                    {
                        dgvFind_Product.Rows[index].Cells["dgvFind_ShowDiscount"].Value = i.Discount;
                    }
                }

            }
        }


        public void deleteDgvPayment(Guna2DataGridView dgvPayment_Product, Guna2TextBox txt_ProductID,
    Guna2TextBox txt_ProductName, Guna2TextBox txt_Quantity)
        {

            if (txt_ProductID.Text != "")
            {
                var t = dgvPayment_Product.Rows.Count;
                for (int i = 0; i < t; i++)
                {
                    var checkdgv_ProductID = dgvPayment_Product.Rows[i].Cells["dgvPay_ProductID"].Value.ToString();
                    if (checkdgv_ProductID == txt_ProductID.Text)
                    {
                        DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa sản phẩm này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {
                            dgvPayment_Product.Rows.RemoveAt(i);
                            txt_Quantity.Text = "";
                            txt_ProductID.Text = "";
                            txt_ProductName.Text = "";
                        }
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập mã sản phẩm cần xóa");
            }
        }
        public string CalculateTotalAmount(Guna2DataGridView dgvPayment_Product , string txt_InputPoint)
        {
            double totalAmount = 0;

            foreach (DataGridViewRow row in dgvPayment_Product.Rows)
            {
                if (row.Cells["dgvPay_SellPrice"].Value != null &&
                    float.TryParse(row.Cells["dgvPay_SellPrice"].Value.ToString(), out float sellPrice)
                    && row.Cells["dgvPay_Quantity"].Value != null &&
                    int.TryParse(row.Cells["dgvPay_Quantity"].Value.ToString(), out int quantity)
                    && row.Cells["dgvPay_ShowDiscount"].Value != null &&
                    double.TryParse(row.Cells["dgvPay_ShowDiscount"].Value.ToString(), out double discount))
                {
                    if (quantity != 0)
                    {
                        var t = sellPrice * quantity - ((sellPrice * quantity) * (discount / 100));
                        if(txt_InputPoint == null)
                        {
                            totalAmount += t;
                        }
                        else 
                        {
                            double.TryParse(txt_InputPoint,out double inputPoint);
                            totalAmount += t - inputPoint;
                        }
                    }
                }
            }

            return totalAmount.ToString();
        }
        static string user;
        public void saveUser(string txt_Username)
        {
            user = txt_Username;
        }
        public void clickPayment(Guna2DataGridView dgvPayment_Product, Guna2DataGridView dgvFind_Product, string txt_InputPoint, string txt_CustomerID, 
            string txt_ShowPoint, Guna2ToggleSwitch cb_AccumulatePoints, Guna2ToggleSwitch cb_UsePoints)
        {
            UpdateDBInvoice_And_DetalInvoice( dgvPayment_Product, txt_InputPoint, txt_CustomerID, txt_ShowPoint, cb_AccumulatePoints, 
                cb_UsePoints);
            LoadDgvFindProduct( dgvFind_Product );
        }
        public void UpdateDBInvoice_And_DetalInvoice(Guna2DataGridView dgvPayment_Product, string txt_InputPoint, 
            string txt_CustomerID, string txt_ShowPoint, Guna2ToggleSwitch cb_AccumulatePoints, Guna2ToggleSwitch cb_UsePoints)
        {
            var listDgvPayment = dgvPayment_Product.Rows.Count;
            if(listDgvPayment != 0) 
            {
                DialogResult result = MessageBox.Show("Bạn có muốn thanh toán?","Xác nhận thanh toán",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if (result == DialogResult.Yes) 
                {
                    //Update DB Invoice
                    var listInvoice = dal_payment.GetAllInvoice();
                    var listCustomer = dal_payment.GetCustomerSearch(txt_CustomerID);
                    var invoice = dal_payment.GetInvoice();
                    int lastInvoiceID = (dal_payment.GetLastIndexOfInvoice() + 1);
                    invoice.InvoiceID = "I" + lastInvoiceID.ToString();
                    //invoice.Date = DateTime.Now.ToString("yyyy-MM-dd");
                    double.TryParse(CalculateTotalAmount(dgvPayment_Product, txt_InputPoint), out double p);
                    invoice.PriceTotal = p;
                    double.TryParse(txt_InputPoint, out double inputPoint);
                    double.TryParse(txt_ShowPoint, out double showPoint);
                    int checkError = 0;
                    if (txt_CustomerID != null)
                    {
                        invoice.CustomerID = txt_CustomerID;
                        if(cb_UsePoints.Checked == true)
                        {
                            if (inputPoint > showPoint || inputPoint < 0)
                            {
                                MessageBox.Show("Điểm tích lũy không đủ. Vui lòng nhập lại!");
                                txt_InputPoint = string.Empty;
                                return;
                            }
                        }
                        else
                        {
                            invoice.MinusPrice = inputPoint;

                        }
                        
                        foreach (var customers in listCustomer)
                        {
                            
                            
                            if(cb_AccumulatePoints.Checked == true)
                            {
                                var listCustomerSearch = dal_payment.GetCustomerSearch(txt_CustomerID);
                                foreach( var customerSearch in listCustomerSearch)
                                {
                                    if (cb_UsePoints.Checked == false)
                                    {
                                        var t = (p * 0.01);
                                        customerSearch.Point = customers.Point + t;
                                        dal_payment.CustomerUpdate(customerSearch);
                                    }
                                    else
                                    {
                                        if (inputPoint == 0)
                                        {
                                            MessageBox.Show("Vui lòng nhập điểm cần sử dụng và điểm phải là số nguyên!");
                                            checkError = 1;
                                            break;
                                        }
                                        else
                                        {
                                            customerSearch.Point -= inputPoint;
                                            dal_payment.CustomerUpdate(customerSearch);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        invoice.CustomerID = null;
                    }
                    if (checkError == 1)
                    {
                        return;
                    }
                    var listAcount = dal_login.GetAccountList();
                    string employeeID = "";
                    foreach (var t in listAcount)
                    {
                        if (t.Username.Trim() == user.Trim())
                        {
                            employeeID = t.EmployeeID.Trim().ToString();
                            break;
                        }
                    }
                    invoice.EmployeeID = employeeID;
                    
                    invoice.CustomerID = txt_CustomerID;
                    dal_payment.InvoiceUpdate(invoice);
                    //Update DB DetailInvoice
                    
                    var detailInvoice = dal_payment.GetDetailInvoice();
                    var listDgvPaymentCount = dgvPayment_Product.Rows.Count;
                    for (int i = 0; i < listDgvPaymentCount; i++)
                    {
                        detailInvoice.InvoiceID = "I" + lastInvoiceID;
                        detailInvoice.Price = dgvPayment_Product.Rows[i].Cells["dgvPay_SellPrice"].Value.ToString();
                        double.TryParse(dgvPayment_Product.Rows[i].Cells["dgvPay_Quantity"].Value.ToString(), out double quantity);
                        detailInvoice.Quantity = quantity;
                        var productID = dgvPayment_Product.Rows[i].Cells["dgvPay_ProductID"].Value.ToString();
                        detailInvoice.ProductID = productID;
                        dal_payment.DetailInvoiceUpdate(detailInvoice);

                        var GetProductID_in_Repository = dal_payment.GetRepositorySearch(productID);
                        foreach (var t in GetProductID_in_Repository)
                        {
                            t.ProductQuantity -= quantity;
                            dal_payment.RepositoryUpdate(t);
                        }

                    }

                    dgvPayment_Product.Rows.Clear();
                    txt_CustomerID = null;
                    txt_ShowPoint = null;
                    txt_InputPoint = null;
                    cb_AccumulatePoints.Checked = false;
                    cb_UsePoints.Checked = false;
                    return;

                }
            }
            else
            {
                MessageBox.Show("Vui lòng thêm sảm phẩm cần thanh toán!");
            }
        }
        public void AccumulatePoints(Guna2ToggleSwitch cb_AccumulatePoints, Guna2TextBox txt_CustomerID, Guna2TextBox txt_ShowPoint, 
            Guna2GradientButton btn_SearchCustomer)
        {
            if(cb_AccumulatePoints.Checked)
            {
                txt_CustomerID.Enabled = true;
                btn_SearchCustomer.Enabled = true;
            }
            else
            {
                txt_CustomerID.Enabled = false;
                txt_CustomerID.Text = "";
                txt_ShowPoint.Text = "";
                btn_SearchCustomer.Enabled = false;
            }

        }
        public void UsePoints(Guna2ToggleSwitch cb_UsePoints, Guna2TextBox txt_InputPoint)
        {
            if (cb_UsePoints.Checked)
            {
                txt_InputPoint.Enabled = true;
            }
            else
            {
                txt_InputPoint.Text = "";
                txt_InputPoint.Enabled = false;
            }

        }
        public void FindCustomerID_And_ShowPoint(Guna2ToggleSwitch cb_AccumulatePoints, 
            Guna2TextBox txt_CustomerID, Guna2TextBox txt_ShowPoint)
        {
            if (cb_AccumulatePoints.Checked)
            {
                var customerID = dal_payment.GetCustomerSearch(txt_CustomerID.Text);
                var customer = dal_payment.GetCustomer();
                var listCustomer = dal_payment.GetAllCustomer();
                int checkNull = 0;
                foreach( var item in customerID.ToList())
                {
                    checkNull++;
                    if (txt_CustomerID == null)
                    {
                        MessageBox.Show("Không tìm thấy mã khách hàng!");
                        txt_CustomerID.Clear();
                        return;
                    }
                    txt_ShowPoint.Text = item.Point.ToString();
                }
                if (checkNull == 0)
                {
                    MessageBox.Show("Chưa có khách hàng nào trong dữ liệu");
                    cb_AccumulatePoints.Checked = false;
                    return;
                }
                
            }
        }
    }
}
