﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_LoyalCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LoyalCustomers));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_LoyalCustomers = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgv_CustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CustomerPhoneNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CustomerPoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CustomerID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ShowPoint = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2GradientButton1 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_addBill = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Delete = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton2 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox5 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox3 = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.guna2AnimateWindow2 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_LoyalCustomers)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.guna2CustomGradientPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_LoyalCustomers
            // 
            this.dgv_LoyalCustomers.AllowUserToAddRows = false;
            this.dgv_LoyalCustomers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_LoyalCustomers.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_LoyalCustomers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_LoyalCustomers.ColumnHeadersHeight = 18;
            this.dgv_LoyalCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_LoyalCustomers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_CustomerID,
            this.dgv_CustomerName,
            this.dgv_CustomerPhoneNum,
            this.dgv_CustomerPoint});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_LoyalCustomers.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_LoyalCustomers.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_LoyalCustomers.Location = new System.Drawing.Point(519, -1);
            this.dgv_LoyalCustomers.Name = "dgv_LoyalCustomers";
            this.dgv_LoyalCustomers.ReadOnly = true;
            this.dgv_LoyalCustomers.RowHeadersVisible = false;
            this.dgv_LoyalCustomers.RowHeadersWidth = 51;
            this.dgv_LoyalCustomers.RowTemplate.Height = 24;
            this.dgv_LoyalCustomers.Size = new System.Drawing.Size(616, 692);
            this.dgv_LoyalCustomers.TabIndex = 5;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_LoyalCustomers.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_LoyalCustomers.ThemeStyle.ReadOnly = true;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // dgv_CustomerID
            // 
            this.dgv_CustomerID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerID.HeaderText = "Mã khách hàng";
            this.dgv_CustomerID.MinimumWidth = 6;
            this.dgv_CustomerID.Name = "dgv_CustomerID";
            this.dgv_CustomerID.ReadOnly = true;
            // 
            // dgv_CustomerName
            // 
            this.dgv_CustomerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerName.HeaderText = "Tên khách hàng";
            this.dgv_CustomerName.MinimumWidth = 6;
            this.dgv_CustomerName.Name = "dgv_CustomerName";
            this.dgv_CustomerName.ReadOnly = true;
            // 
            // dgv_CustomerPhoneNum
            // 
            this.dgv_CustomerPhoneNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerPhoneNum.HeaderText = "Số điện thoại";
            this.dgv_CustomerPhoneNum.MinimumWidth = 6;
            this.dgv_CustomerPhoneNum.Name = "dgv_CustomerPhoneNum";
            this.dgv_CustomerPhoneNum.ReadOnly = true;
            // 
            // dgv_CustomerPoint
            // 
            this.dgv_CustomerPoint.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerPoint.HeaderText = "Điểm tích lũy";
            this.dgv_CustomerPoint.MinimumWidth = 6;
            this.dgv_CustomerPoint.Name = "dgv_CustomerPoint";
            this.dgv_CustomerPoint.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.guna2TextBox2);
            this.groupBox2.Controls.Add(this.guna2TextBox1);
            this.groupBox2.Controls.Add(this.txt_CustomerID);
            this.groupBox2.Controls.Add(this.txt_ShowPoint);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 215);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(489, 327);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin khách hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(37, 193);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số điện thoại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(37, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên khách hàng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(37, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Mã khách hàng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(37, 263);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "Số điểm";
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.Animated = true;
            this.guna2TextBox2.BackColor = System.Drawing.Color.Transparent;
            this.guna2TextBox2.BorderColor = System.Drawing.Color.LightGray;
            this.guna2TextBox2.BorderRadius = 15;
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.Enabled = false;
            this.guna2TextBox2.FillColor = System.Drawing.Color.WhiteSmoke;
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.guna2TextBox2.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.guna2TextBox2.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.guna2TextBox2.Font = new System.Drawing.Font("Tahoma", 9F);
            this.guna2TextBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.guna2TextBox2.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.guna2TextBox2.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.guna2TextBox2.Location = new System.Drawing.Point(192, 185);
            this.guna2TextBox2.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.guna2TextBox2.PlaceholderText = "";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.ShadowDecoration.BorderRadius = 15;
            this.guna2TextBox2.ShadowDecoration.Enabled = true;
            this.guna2TextBox2.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.guna2TextBox2.Size = new System.Drawing.Size(263, 35);
            this.guna2TextBox2.TabIndex = 3;
            // 
            // guna2TextBox1
            // 
            this.guna2TextBox1.Animated = true;
            this.guna2TextBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2TextBox1.BorderColor = System.Drawing.Color.LightGray;
            this.guna2TextBox1.BorderRadius = 15;
            this.guna2TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox1.DefaultText = "";
            this.guna2TextBox1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.Enabled = false;
            this.guna2TextBox1.FillColor = System.Drawing.Color.WhiteSmoke;
            this.guna2TextBox1.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.guna2TextBox1.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.guna2TextBox1.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.guna2TextBox1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.guna2TextBox1.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox1.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.guna2TextBox1.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.guna2TextBox1.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.guna2TextBox1.Location = new System.Drawing.Point(192, 115);
            this.guna2TextBox1.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.guna2TextBox1.Name = "guna2TextBox1";
            this.guna2TextBox1.PasswordChar = '\0';
            this.guna2TextBox1.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.guna2TextBox1.PlaceholderText = "";
            this.guna2TextBox1.SelectedText = "";
            this.guna2TextBox1.ShadowDecoration.BorderRadius = 15;
            this.guna2TextBox1.ShadowDecoration.Enabled = true;
            this.guna2TextBox1.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.guna2TextBox1.Size = new System.Drawing.Size(263, 35);
            this.guna2TextBox1.TabIndex = 3;
            // 
            // txt_CustomerID
            // 
            this.txt_CustomerID.Animated = true;
            this.txt_CustomerID.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.BorderRadius = 15;
            this.txt_CustomerID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerID.DefaultText = "";
            this.txt_CustomerID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.Enabled = false;
            this.txt_CustomerID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerID.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.Location = new System.Drawing.Point(192, 48);
            this.txt_CustomerID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerID.Name = "txt_CustomerID";
            this.txt_CustomerID.PasswordChar = '\0';
            this.txt_CustomerID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.PlaceholderText = "";
            this.txt_CustomerID.SelectedText = "";
            this.txt_CustomerID.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerID.ShadowDecoration.Enabled = true;
            this.txt_CustomerID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerID.Size = new System.Drawing.Size(263, 35);
            this.txt_CustomerID.TabIndex = 3;
            // 
            // txt_ShowPoint
            // 
            this.txt_ShowPoint.Animated = true;
            this.txt_ShowPoint.BackColor = System.Drawing.Color.Transparent;
            this.txt_ShowPoint.BorderColor = System.Drawing.Color.LightGray;
            this.txt_ShowPoint.BorderRadius = 15;
            this.txt_ShowPoint.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ShowPoint.DefaultText = "";
            this.txt_ShowPoint.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ShowPoint.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ShowPoint.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ShowPoint.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ShowPoint.Enabled = false;
            this.txt_ShowPoint.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ShowPoint.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ShowPoint.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ShowPoint.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ShowPoint.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ShowPoint.ForeColor = System.Drawing.Color.Black;
            this.txt_ShowPoint.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ShowPoint.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ShowPoint.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ShowPoint.Location = new System.Drawing.Point(192, 253);
            this.txt_ShowPoint.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ShowPoint.Name = "txt_ShowPoint";
            this.txt_ShowPoint.PasswordChar = '\0';
            this.txt_ShowPoint.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ShowPoint.PlaceholderText = "";
            this.txt_ShowPoint.SelectedText = "";
            this.txt_ShowPoint.ShadowDecoration.BorderRadius = 15;
            this.txt_ShowPoint.ShadowDecoration.Enabled = true;
            this.txt_ShowPoint.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ShowPoint.Size = new System.Drawing.Size(167, 35);
            this.txt_ShowPoint.TabIndex = 3;
            // 
            // guna2GradientButton1
            // 
            this.guna2GradientButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2GradientButton1.BorderRadius = 10;
            this.guna2GradientButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.guna2GradientButton1.FillColor2 = System.Drawing.Color.Tomato;
            this.guna2GradientButton1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.guna2GradientButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientButton1.Image = ((System.Drawing.Image)(resources.GetObject("guna2GradientButton1.Image")));
            this.guna2GradientButton1.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2GradientButton1.Location = new System.Drawing.Point(301, 557);
            this.guna2GradientButton1.Name = "guna2GradientButton1";
            this.guna2GradientButton1.ShadowDecoration.BorderRadius = 10;
            this.guna2GradientButton1.ShadowDecoration.Enabled = true;
            this.guna2GradientButton1.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.guna2GradientButton1.Size = new System.Drawing.Size(177, 45);
            this.guna2GradientButton1.TabIndex = 20;
            this.guna2GradientButton1.Text = "Sửa thông tin";
            this.guna2GradientButton1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_addBill
            // 
            this.btn_addBill.BackColor = System.Drawing.Color.Transparent;
            this.btn_addBill.BorderRadius = 10;
            this.btn_addBill.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_addBill.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_addBill.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addBill.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addBill.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_addBill.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_addBill.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_addBill.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_addBill.ForeColor = System.Drawing.Color.White;
            this.btn_addBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_addBill.Image")));
            this.btn_addBill.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_addBill.Location = new System.Drawing.Point(53, 557);
            this.btn_addBill.Name = "btn_addBill";
            this.btn_addBill.ShadowDecoration.BorderRadius = 10;
            this.btn_addBill.ShadowDecoration.Enabled = true;
            this.btn_addBill.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_addBill.Size = new System.Drawing.Size(192, 45);
            this.btn_addBill.TabIndex = 20;
            this.btn_addBill.Text = "Thêm khách hàng";
            this.btn_addBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_Delete.BorderRadius = 10;
            this.btn_Delete.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Delete.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Delete.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Delete.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Delete.ForeColor = System.Drawing.Color.White;
            this.btn_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btn_Delete.Image")));
            this.btn_Delete.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Delete.Location = new System.Drawing.Point(54, 625);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.ShadowDecoration.BorderRadius = 10;
            this.btn_Delete.ShadowDecoration.Enabled = true;
            this.btn_Delete.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Delete.Size = new System.Drawing.Size(191, 45);
            this.btn_Delete.TabIndex = 21;
            this.btn_Delete.Text = "Xóa khách hàng";
            this.btn_Delete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // guna2GradientButton2
            // 
            this.guna2GradientButton2.BackColor = System.Drawing.Color.Transparent;
            this.guna2GradientButton2.BorderRadius = 10;
            this.guna2GradientButton2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.guna2GradientButton2.FillColor2 = System.Drawing.Color.Tomato;
            this.guna2GradientButton2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.guna2GradientButton2.ForeColor = System.Drawing.Color.White;
            this.guna2GradientButton2.Image = ((System.Drawing.Image)(resources.GetObject("guna2GradientButton2.Image")));
            this.guna2GradientButton2.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2GradientButton2.Location = new System.Drawing.Point(301, 625);
            this.guna2GradientButton2.Name = "guna2GradientButton2";
            this.guna2GradientButton2.ShadowDecoration.BorderRadius = 10;
            this.guna2GradientButton2.ShadowDecoration.Enabled = true;
            this.guna2GradientButton2.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.guna2GradientButton2.Size = new System.Drawing.Size(177, 45);
            this.guna2GradientButton2.TabIndex = 21;
            this.guna2GradientButton2.Text = "Xuất danh sách";
            this.guna2GradientButton2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.LightPink;
            this.guna2CustomGradientPanel1.BorderRadius = 25;
            this.guna2CustomGradientPanel1.BorderThickness = 2;
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2TextBox4);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2TextBox5);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2TextBox3);
            this.guna2CustomGradientPanel1.Controls.Add(this.label4);
            this.guna2CustomGradientPanel1.Controls.Add(this.label3);
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(30, 12);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(471, 183);
            this.guna2CustomGradientPanel1.TabIndex = 22;
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Animated = true;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.Enabled = false;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(936, 65);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.White;
            this.guna2TextBox4.PlaceholderText = "";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(515, 326);
            this.guna2TextBox4.TabIndex = 1;
            // 
            // guna2TextBox5
            // 
            this.guna2TextBox5.Animated = true;
            this.guna2TextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox5.DefaultText = "";
            this.guna2TextBox5.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.Enabled = false;
            this.guna2TextBox5.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox5.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.Location = new System.Drawing.Point(271, 19);
            this.guna2TextBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2TextBox5.Name = "guna2TextBox5";
            this.guna2TextBox5.PasswordChar = '\0';
            this.guna2TextBox5.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.PlaceholderText = "0";
            this.guna2TextBox5.SelectedText = "";
            this.guna2TextBox5.Size = new System.Drawing.Size(149, 95);
            this.guna2TextBox5.TabIndex = 1;
            this.guna2TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // guna2TextBox3
            // 
            this.guna2TextBox3.Animated = true;
            this.guna2TextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox3.DefaultText = "";
            this.guna2TextBox3.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.Enabled = false;
            this.guna2TextBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2TextBox3.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.Location = new System.Drawing.Point(51, 19);
            this.guna2TextBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.guna2TextBox3.Name = "guna2TextBox3";
            this.guna2TextBox3.PasswordChar = '\0';
            this.guna2TextBox3.PlaceholderForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.PlaceholderText = "0";
            this.guna2TextBox3.SelectedText = "";
            this.guna2TextBox3.Size = new System.Drawing.Size(149, 95);
            this.guna2TextBox3.TabIndex = 1;
            this.guna2TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(288, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Khách vãng lai";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(41, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Khách hàng thân thiết";
            // 
            // frm_LoyalCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.guna2CustomGradientPanel1);
            this.Controls.Add(this.guna2GradientButton2);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.guna2GradientButton1);
            this.Controls.Add(this.btn_addBill);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgv_LoyalCustomers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_LoyalCustomers";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_LoyalCustomers)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.guna2CustomGradientPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_LoyalCustomers;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerPhoneNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerPoint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerID;
        private Guna.UI2.WinForms.Guna2TextBox txt_ShowPoint;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton1;
        private Guna.UI2.WinForms.Guna2GradientButton btn_addBill;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Delete;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox3;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow2;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox5;
        private System.Windows.Forms.Label label4;
    }
}