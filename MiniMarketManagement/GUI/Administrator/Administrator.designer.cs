﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin));
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_AdminSideBoard = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2Button7 = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Exit = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ReturnManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ImportManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_LoyalCustomers = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Invoices = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Statistics = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ImageButton1 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2DragControl2 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_Main = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.panel_AdminSideBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl1.TargetControl = this.panel_AdminSideBoard;
            this.guna2DragControl1.TransparentWhileDrag = false;
            // 
            // panel_AdminSideBoard
            // 
            this.panel_AdminSideBoard.BackColor = System.Drawing.Color.Transparent;
            this.panel_AdminSideBoard.BorderRadius = 10;
            this.panel_AdminSideBoard.Controls.Add(this.guna2Button7);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Exit);
            this.panel_AdminSideBoard.Controls.Add(this.btn_ReturnManagement);
            this.panel_AdminSideBoard.Controls.Add(this.btn_ImportManagement);
            this.panel_AdminSideBoard.Controls.Add(this.btn_LoyalCustomers);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Invoices);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Statistics);
            this.panel_AdminSideBoard.Controls.Add(this.guna2ImageButton1);
            this.panel_AdminSideBoard.Controls.Add(this.guna2Separator2);
            this.panel_AdminSideBoard.Controls.Add(this.guna2Separator1);
            this.panel_AdminSideBoard.FillColor = System.Drawing.Color.HotPink;
            this.panel_AdminSideBoard.FillColor2 = System.Drawing.Color.Red;
            this.panel_AdminSideBoard.FillColor3 = System.Drawing.Color.LightCyan;
            this.panel_AdminSideBoard.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_AdminSideBoard.Location = new System.Drawing.Point(-19, -1);
            this.panel_AdminSideBoard.Name = "panel_AdminSideBoard";
            this.panel_AdminSideBoard.ShadowDecoration.Depth = 10;
            this.panel_AdminSideBoard.ShadowDecoration.Enabled = true;
            this.panel_AdminSideBoard.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_AdminSideBoard.Size = new System.Drawing.Size(258, 696);
            this.panel_AdminSideBoard.TabIndex = 0;
            // 
            // guna2Button7
            // 
            this.guna2Button7.Animated = true;
            this.guna2Button7.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button7.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button7.BorderRadius = 20;
            this.guna2Button7.BorderThickness = 2;
            this.guna2Button7.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button7.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button7.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button7.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button7.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2Button7.ForeColor = System.Drawing.Color.White;
            this.guna2Button7.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button7.Image = global::MiniMarketManagement.Properties.Resources.zoom_out;
            this.guna2Button7.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2Button7.ImageOffset = new System.Drawing.Point(2, 0);
            this.guna2Button7.ImageSize = new System.Drawing.Size(31, 31);
            this.guna2Button7.IndicateFocus = true;
            this.guna2Button7.Location = new System.Drawing.Point(28, 572);
            this.guna2Button7.Name = "guna2Button7";
            this.guna2Button7.ShadowDecoration.Depth = 1;
            this.guna2Button7.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button7.Size = new System.Drawing.Size(219, 55);
            this.guna2Button7.TabIndex = 2;
            this.guna2Button7.Text = "Thu nhỏ";
            this.guna2Button7.UseTransparentBackground = true;
            // 
            // btn_Exit
            // 
            this.btn_Exit.Animated = true;
            this.btn_Exit.BackColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderRadius = 20;
            this.btn_Exit.BorderThickness = 2;
            this.btn_Exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Exit.FillColor = System.Drawing.Color.Transparent;
            this.btn_Exit.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Exit.ForeColor = System.Drawing.Color.White;
            this.btn_Exit.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Exit.Image = global::MiniMarketManagement.Properties.Resources.exit__1_;
            this.btn_Exit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Exit.ImageOffset = new System.Drawing.Point(6, 0);
            this.btn_Exit.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Exit.IndicateFocus = true;
            this.btn_Exit.Location = new System.Drawing.Point(28, 633);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.ShadowDecoration.Depth = 1;
            this.btn_Exit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Exit.Size = new System.Drawing.Size(219, 55);
            this.btn_Exit.TabIndex = 2;
            this.btn_Exit.Text = "Thoát";
            this.btn_Exit.UseTransparentBackground = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_ReturnManagement
            // 
            this.btn_ReturnManagement.Animated = true;
            this.btn_ReturnManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.BorderRadius = 20;
            this.btn_ReturnManagement.BorderThickness = 2;
            this.btn_ReturnManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ReturnManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ReturnManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ReturnManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ReturnManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ReturnManagement.ForeColor = System.Drawing.Color.White;
            this.btn_ReturnManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ReturnManagement.IndicateFocus = true;
            this.btn_ReturnManagement.Location = new System.Drawing.Point(28, 371);
            this.btn_ReturnManagement.Name = "btn_ReturnManagement";
            this.btn_ReturnManagement.ShadowDecoration.Depth = 1;
            this.btn_ReturnManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ReturnManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_ReturnManagement.TabIndex = 2;
            this.btn_ReturnManagement.Text = "Trả hàng";
            this.btn_ReturnManagement.UseTransparentBackground = true;
            this.btn_ReturnManagement.Click += new System.EventHandler(this.btn_ReturnManagement_Click);
            // 
            // btn_ImportManagement
            // 
            this.btn_ImportManagement.Animated = true;
            this.btn_ImportManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.BorderRadius = 20;
            this.btn_ImportManagement.BorderThickness = 2;
            this.btn_ImportManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ImportManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ImportManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportManagement.ForeColor = System.Drawing.Color.White;
            this.btn_ImportManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ImportManagement.IndicateFocus = true;
            this.btn_ImportManagement.Location = new System.Drawing.Point(28, 310);
            this.btn_ImportManagement.Name = "btn_ImportManagement";
            this.btn_ImportManagement.ShadowDecoration.Depth = 1;
            this.btn_ImportManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ImportManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_ImportManagement.TabIndex = 2;
            this.btn_ImportManagement.Text = "Nhập hàng";
            this.btn_ImportManagement.UseTransparentBackground = true;
            this.btn_ImportManagement.Click += new System.EventHandler(this.btn_ImportManagement_Click);
            // 
            // btn_LoyalCustomers
            // 
            this.btn_LoyalCustomers.Animated = true;
            this.btn_LoyalCustomers.BackColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.BorderColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.BorderRadius = 20;
            this.btn_LoyalCustomers.BorderThickness = 2;
            this.btn_LoyalCustomers.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_LoyalCustomers.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_LoyalCustomers.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_LoyalCustomers.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_LoyalCustomers.FillColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LoyalCustomers.ForeColor = System.Drawing.Color.White;
            this.btn_LoyalCustomers.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_LoyalCustomers.IndicateFocus = true;
            this.btn_LoyalCustomers.Location = new System.Drawing.Point(28, 249);
            this.btn_LoyalCustomers.Name = "btn_LoyalCustomers";
            this.btn_LoyalCustomers.ShadowDecoration.Depth = 1;
            this.btn_LoyalCustomers.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_LoyalCustomers.Size = new System.Drawing.Size(219, 55);
            this.btn_LoyalCustomers.TabIndex = 2;
            this.btn_LoyalCustomers.Text = "Khách hàng";
            this.btn_LoyalCustomers.UseTransparentBackground = true;
            this.btn_LoyalCustomers.Click += new System.EventHandler(this.btn_LoyalCustomers_Click);
            // 
            // btn_Invoices
            // 
            this.btn_Invoices.Animated = true;
            this.btn_Invoices.BackColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.BorderRadius = 20;
            this.btn_Invoices.BorderThickness = 2;
            this.btn_Invoices.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Invoices.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Invoices.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Invoices.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Invoices.FillColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Invoices.ForeColor = System.Drawing.Color.White;
            this.btn_Invoices.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Invoices.IndicateFocus = true;
            this.btn_Invoices.Location = new System.Drawing.Point(28, 188);
            this.btn_Invoices.Name = "btn_Invoices";
            this.btn_Invoices.ShadowDecoration.Depth = 1;
            this.btn_Invoices.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Invoices.Size = new System.Drawing.Size(219, 55);
            this.btn_Invoices.TabIndex = 2;
            this.btn_Invoices.Text = "Hóa đơn";
            this.btn_Invoices.UseTransparentBackground = true;
            this.btn_Invoices.Click += new System.EventHandler(this.btn_Invoices_Click);
            // 
            // btn_Statistics
            // 
            this.btn_Statistics.Animated = true;
            this.btn_Statistics.BackColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.BorderRadius = 20;
            this.btn_Statistics.BorderThickness = 2;
            this.btn_Statistics.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Statistics.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Statistics.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Statistics.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Statistics.FillColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Statistics.ForeColor = System.Drawing.Color.White;
            this.btn_Statistics.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Statistics.IndicateFocus = true;
            this.btn_Statistics.Location = new System.Drawing.Point(28, 127);
            this.btn_Statistics.Name = "btn_Statistics";
            this.btn_Statistics.ShadowDecoration.Depth = 1;
            this.btn_Statistics.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Statistics.Size = new System.Drawing.Size(219, 55);
            this.btn_Statistics.TabIndex = 2;
            this.btn_Statistics.Text = "Thống kê";
            this.btn_Statistics.UseTransparentBackground = true;
            this.btn_Statistics.Click += new System.EventHandler(this.btn_Statistics_Click);
            // 
            // guna2ImageButton1
            // 
            this.guna2ImageButton1.AnimatedGIF = true;
            this.guna2ImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ImageButton1.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton1.HoverState.ImageSize = new System.Drawing.Size(100, 100);
            this.guna2ImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("guna2ImageButton1.Image")));
            this.guna2ImageButton1.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton1.ImageRotate = 0F;
            this.guna2ImageButton1.ImageSize = new System.Drawing.Size(90, 90);
            this.guna2ImageButton1.Location = new System.Drawing.Point(82, 3);
            this.guna2ImageButton1.Name = "guna2ImageButton1";
            this.guna2ImageButton1.PressedState.ImageSize = new System.Drawing.Size(95, 95);
            this.guna2ImageButton1.Size = new System.Drawing.Size(111, 118);
            this.guna2ImageButton1.TabIndex = 6;
            this.guna2ImageButton1.UseTransparentBackground = true;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator2.Location = new System.Drawing.Point(29, 556);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator2.TabIndex = 5;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator1.Location = new System.Drawing.Point(31, 113);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator1.TabIndex = 1;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2DragControl2
            // 
            this.guna2DragControl2.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl2.TargetControl = this.panel_Main;
            this.guna2DragControl2.TransparentWhileDrag = false;
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.panel_Main.FillColor = System.Drawing.Color.White;
            this.panel_Main.Location = new System.Drawing.Point(244, -1);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Radius = 5;
            this.panel_Main.ShadowColor = System.Drawing.Color.Black;
            this.panel_Main.ShadowDepth = 95;
            this.panel_Main.ShadowShift = 8;
            this.panel_Main.Size = new System.Drawing.Size(1139, 696);
            this.panel_Main.TabIndex = 3;
            // 
            // frm_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1384, 695);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.panel_AdminSideBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Admin";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.frm_Admin_Load);
            this.panel_AdminSideBoard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_AdminSideBoard;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_Main;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton1;
        private Guna.UI2.WinForms.Guna2Button btn_Statistics;
        private Guna.UI2.WinForms.Guna2Button btn_ReturnManagement;
        private Guna.UI2.WinForms.Guna2Button btn_ImportManagement;
        private Guna.UI2.WinForms.Guna2Button btn_LoyalCustomers;
        private Guna.UI2.WinForms.Guna2Button btn_Exit;
        private Guna.UI2.WinForms.Guna2Button guna2Button7;
        private Guna.UI2.WinForms.Guna2Button btn_Invoices;
    }
}

