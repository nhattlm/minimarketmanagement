﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_Admin : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        public frm_Admin()
        {
            InitializeComponent();
        }
        private void frm_Admin_Load(object sender, EventArgs e)
        {
            btn_Statistics_Click(sender, e);
        }
        private void btn_Statistics_Click(object sender, EventArgs e)
        {
            frm_Statistics formStatistic = new frm_Statistics();
            showChildrenForm.ShowForm(formStatistic, btn_Statistics, panel_Main);
        }

        private void btn_Invoices_Click(object sender, EventArgs e)
        {
            frm_Invoices formInvoices = new frm_Invoices();
            showChildrenForm.ShowForm(formInvoices, btn_Invoices, panel_Main);
        }

        private void btn_LoyalCustomers_Click(object sender, EventArgs e)
        {
            frm_LoyalCustomers formLoyalCustomer = new frm_LoyalCustomers();
            showChildrenForm.ShowForm(formLoyalCustomer, btn_LoyalCustomers, panel_Main);
        }

        private void btn_ImportManagement_Click(object sender, EventArgs e)
        {
            frm_ImportManagement formImportManagement = new frm_ImportManagement();
            showChildrenForm.ShowForm(formImportManagement, btn_ImportManagement, panel_Main);
        }

        private void btn_ReturnManagement_Click(object sender, EventArgs e)
        {
            frm_ReturnManagement formReturnManagement = new frm_ReturnManagement();
            showChildrenForm.ShowForm(formReturnManagement, btn_ReturnManagement, panel_Main);
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
