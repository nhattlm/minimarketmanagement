﻿using Guna.UI2.WinForms;
using MiniMarketManagement.GUI.Storekeeper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Cashier
{
    public partial class frm_Cashier : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        public frm_Cashier()
        {
            InitializeComponent();
        }
        private void frm_Cashier_Load(object sender, EventArgs e)
        {
            btn_Payment_Click(sender, e);
        }
        private void btn_Payment_Click(object sender, EventArgs e)
        {
            frm_FormPayment formPayment = new frm_FormPayment();
            showChildrenForm.ShowForm(formPayment, btn_Payment, panel_Main);
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {         
            frm_Register formRegister = new frm_Register();
            showChildrenForm.ShowForm(formRegister, btn_Register, panel_Main);
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
