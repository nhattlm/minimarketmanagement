﻿namespace MiniMarketManagement.GUI.Cashier
{
    partial class frm_Cashier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Cashier));
            this.panel_Main = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2DragControl2 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_CashierSideBoard = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Register = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Payment = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Zoom = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Exit = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ImageButton1 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.panel_CashierSideBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.panel_Main.FillColor = System.Drawing.Color.White;
            this.panel_Main.Location = new System.Drawing.Point(244, -1);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Radius = 5;
            this.panel_Main.ShadowColor = System.Drawing.Color.Black;
            this.panel_Main.ShadowDepth = 95;
            this.panel_Main.ShadowShift = 8;
            this.panel_Main.Size = new System.Drawing.Size(1139, 696);
            this.panel_Main.TabIndex = 4;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2DragControl2
            // 
            this.guna2DragControl2.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl2.TargetControl = this.panel_CashierSideBoard;
            this.guna2DragControl2.TransparentWhileDrag = false;
            // 
            // panel_CashierSideBoard
            // 
            this.panel_CashierSideBoard.BackColor = System.Drawing.Color.Transparent;
            this.panel_CashierSideBoard.BorderRadius = 10;
            this.panel_CashierSideBoard.Controls.Add(this.guna2Separator2);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Separator1);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Zoom);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Exit);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Button5);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Button4);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Button3);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Register);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Payment);
            this.panel_CashierSideBoard.Controls.Add(this.guna2ImageButton1);
            this.panel_CashierSideBoard.FillColor = System.Drawing.Color.HotPink;
            this.panel_CashierSideBoard.FillColor2 = System.Drawing.Color.Red;
            this.panel_CashierSideBoard.FillColor3 = System.Drawing.Color.LightCyan;
            this.panel_CashierSideBoard.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_CashierSideBoard.Location = new System.Drawing.Point(-19, -1);
            this.panel_CashierSideBoard.Name = "panel_CashierSideBoard";
            this.panel_CashierSideBoard.ShadowDecoration.Depth = 10;
            this.panel_CashierSideBoard.ShadowDecoration.Enabled = true;
            this.panel_CashierSideBoard.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_CashierSideBoard.Size = new System.Drawing.Size(258, 696);
            this.panel_CashierSideBoard.TabIndex = 1;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator2.Location = new System.Drawing.Point(29, 556);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator2.TabIndex = 3;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator1.Location = new System.Drawing.Point(31, 113);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator1.TabIndex = 2;
            // 
            // guna2Button5
            // 
            this.guna2Button5.Animated = true;
            this.guna2Button5.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button5.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button5.BorderRadius = 20;
            this.guna2Button5.BorderThickness = 2;
            this.guna2Button5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button5.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button5.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button5.IndicateFocus = true;
            this.guna2Button5.Location = new System.Drawing.Point(28, 371);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.ShadowDecoration.Depth = 1;
            this.guna2Button5.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button5.Size = new System.Drawing.Size(219, 55);
            this.guna2Button5.TabIndex = 1;
            this.guna2Button5.Text = "guna2Button1";
            this.guna2Button5.UseTransparentBackground = true;
            // 
            // guna2Button4
            // 
            this.guna2Button4.Animated = true;
            this.guna2Button4.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button4.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button4.BorderRadius = 20;
            this.guna2Button4.BorderThickness = 2;
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button4.IndicateFocus = true;
            this.guna2Button4.Location = new System.Drawing.Point(28, 310);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.ShadowDecoration.Depth = 1;
            this.guna2Button4.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button4.Size = new System.Drawing.Size(219, 55);
            this.guna2Button4.TabIndex = 1;
            this.guna2Button4.Text = "guna2Button1";
            this.guna2Button4.UseTransparentBackground = true;
            // 
            // guna2Button3
            // 
            this.guna2Button3.Animated = true;
            this.guna2Button3.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button3.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button3.BorderRadius = 20;
            this.guna2Button3.BorderThickness = 2;
            this.guna2Button3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button3.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button3.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button3.ForeColor = System.Drawing.Color.White;
            this.guna2Button3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button3.IndicateFocus = true;
            this.guna2Button3.Location = new System.Drawing.Point(28, 249);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.ShadowDecoration.Depth = 1;
            this.guna2Button3.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button3.Size = new System.Drawing.Size(219, 55);
            this.guna2Button3.TabIndex = 1;
            this.guna2Button3.Text = "guna2Button1";
            this.guna2Button3.UseTransparentBackground = true;
            // 
            // btn_Register
            // 
            this.btn_Register.Animated = true;
            this.btn_Register.BackColor = System.Drawing.Color.Transparent;
            this.btn_Register.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Register.BorderRadius = 20;
            this.btn_Register.BorderThickness = 2;
            this.btn_Register.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Register.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Register.FillColor = System.Drawing.Color.Transparent;
            this.btn_Register.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Register.ForeColor = System.Drawing.Color.White;
            this.btn_Register.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Register.IndicateFocus = true;
            this.btn_Register.Location = new System.Drawing.Point(28, 188);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.ShadowDecoration.Depth = 1;
            this.btn_Register.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Register.Size = new System.Drawing.Size(219, 55);
            this.btn_Register.TabIndex = 1;
            this.btn_Register.Text = "Đăng ký";
            this.btn_Register.UseTransparentBackground = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // btn_Payment
            // 
            this.btn_Payment.Animated = true;
            this.btn_Payment.BackColor = System.Drawing.Color.Transparent;
            this.btn_Payment.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Payment.BorderRadius = 20;
            this.btn_Payment.BorderThickness = 2;
            this.btn_Payment.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Payment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Payment.FillColor = System.Drawing.Color.Transparent;
            this.btn_Payment.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payment.ForeColor = System.Drawing.Color.White;
            this.btn_Payment.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Payment.IndicateFocus = true;
            this.btn_Payment.Location = new System.Drawing.Point(28, 127);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.ShadowDecoration.Depth = 1;
            this.btn_Payment.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Payment.Size = new System.Drawing.Size(219, 55);
            this.btn_Payment.TabIndex = 1;
            this.btn_Payment.Text = "Thanh toán";
            this.btn_Payment.UseTransparentBackground = true;
            this.btn_Payment.Click += new System.EventHandler(this.btn_Payment_Click);
            // 
            // btn_Zoom
            // 
            this.btn_Zoom.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btn_Zoom.Animated = true;
            this.btn_Zoom.BackColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.BorderRadius = 20;
            this.btn_Zoom.BorderThickness = 2;
            this.btn_Zoom.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Zoom.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Zoom.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Zoom.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Zoom.FillColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Zoom.ForeColor = System.Drawing.Color.White;
            this.btn_Zoom.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Zoom.HoverState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Zoom.Image = global::MiniMarketManagement.Properties.Resources.zoom_out;
            this.btn_Zoom.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Zoom.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Zoom.ImageSize = new System.Drawing.Size(31, 31);
            this.btn_Zoom.IndicateFocus = true;
            this.btn_Zoom.Location = new System.Drawing.Point(28, 572);
            this.btn_Zoom.Name = "btn_Zoom";
            this.btn_Zoom.ShadowDecoration.Depth = 1;
            this.btn_Zoom.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Zoom.Size = new System.Drawing.Size(219, 55);
            this.btn_Zoom.TabIndex = 1;
            this.btn_Zoom.Text = "Thu Nhỏ";
            this.btn_Zoom.UseTransparentBackground = true;
            // 
            // btn_Exit
            // 
            this.btn_Exit.Animated = true;
            this.btn_Exit.BackColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderRadius = 20;
            this.btn_Exit.BorderThickness = 2;
            this.btn_Exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Exit.FillColor = System.Drawing.Color.Transparent;
            this.btn_Exit.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Exit.ForeColor = System.Drawing.Color.White;
            this.btn_Exit.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Exit.Image = global::MiniMarketManagement.Properties.Resources.exit__1_;
            this.btn_Exit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Exit.ImageOffset = new System.Drawing.Point(6, 0);
            this.btn_Exit.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Exit.IndicateFocus = true;
            this.btn_Exit.Location = new System.Drawing.Point(28, 633);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.ShadowDecoration.Depth = 1;
            this.btn_Exit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Exit.Size = new System.Drawing.Size(219, 55);
            this.btn_Exit.TabIndex = 1;
            this.btn_Exit.Text = "Thoát";
            this.btn_Exit.UseTransparentBackground = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // guna2ImageButton1
            // 
            this.guna2ImageButton1.AnimatedGIF = true;
            this.guna2ImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ImageButton1.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton1.HoverState.ImageSize = new System.Drawing.Size(100, 100);
            this.guna2ImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("guna2ImageButton1.Image")));
            this.guna2ImageButton1.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton1.ImageRotate = 0F;
            this.guna2ImageButton1.ImageSize = new System.Drawing.Size(90, 90);
            this.guna2ImageButton1.Location = new System.Drawing.Point(82, 3);
            this.guna2ImageButton1.Name = "guna2ImageButton1";
            this.guna2ImageButton1.PressedState.ImageSize = new System.Drawing.Size(95, 95);
            this.guna2ImageButton1.Size = new System.Drawing.Size(111, 118);
            this.guna2ImageButton1.TabIndex = 1;
            this.guna2ImageButton1.UseTransparentBackground = true;
            // 
            // frm_Cashier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1384, 695);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.panel_CashierSideBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Cashier";
            this.Text = "Cashier";
            this.Load += new System.EventHandler(this.frm_Cashier_Load);
            this.panel_CashierSideBoard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_CashierSideBoard;
        private Guna.UI2.WinForms.Guna2Button btn_Zoom;
        private Guna.UI2.WinForms.Guna2Button btn_Exit;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button btn_Register;
        private Guna.UI2.WinForms.Guna2Button btn_Payment;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton1;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_Main;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
    }
}