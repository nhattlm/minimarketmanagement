﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_WarehouseManagement : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        public frm_WarehouseManagement()
        {
            InitializeComponent();
        }
        private void frm_WarehouseManagement_Load(object sender, EventArgs e)
        {
            btn_DisplayProduct_Click(sender, e);
        }
        private void btn_DisplayProduct_Click(object sender, EventArgs e)
        {
            frm_DisplayProduct formDisplayProduct = new frm_DisplayProduct();
            showChildrenForm.ShowForm(formDisplayProduct, btn_DisplayProduct, panel_Main);
        }

        private void btn_ImportProduct_Click(object sender, EventArgs e)
        {
            frm_ImportProduct formImportProduct = new frm_ImportProduct();
            showChildrenForm.ShowForm(formImportProduct, btn_ImportProduct, panel_Main);
        }

        private void btn_ProductManagement_Click(object sender, EventArgs e)
        {
            frm_ProductManager formProductManagement = new frm_ProductManager();
            showChildrenForm.ShowForm(formProductManagement, btn_ProductManagement, panel_Main);
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
