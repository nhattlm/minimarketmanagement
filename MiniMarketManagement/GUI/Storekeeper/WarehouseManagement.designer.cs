﻿namespace MiniMarketManagement.GUI.Storekeeper
{
    partial class frm_WarehouseManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_WarehouseManagement));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.panel_StorekeeperSideBoard = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2Button5 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ProductManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ImportProduct = new Guna.UI2.WinForms.Guna2Button();
            this.btn_DisplayProduct = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.panel_Main = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.btn_Exit = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Zoom = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ImageButton1 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.panel_StorekeeperSideBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // panel_StorekeeperSideBoard
            // 
            this.panel_StorekeeperSideBoard.BackColor = System.Drawing.Color.Transparent;
            this.panel_StorekeeperSideBoard.BorderRadius = 10;
            this.panel_StorekeeperSideBoard.Controls.Add(this.btn_Exit);
            this.panel_StorekeeperSideBoard.Controls.Add(this.btn_Zoom);
            this.panel_StorekeeperSideBoard.Controls.Add(this.guna2Button5);
            this.panel_StorekeeperSideBoard.Controls.Add(this.guna2Button4);
            this.panel_StorekeeperSideBoard.Controls.Add(this.btn_ProductManagement);
            this.panel_StorekeeperSideBoard.Controls.Add(this.btn_ImportProduct);
            this.panel_StorekeeperSideBoard.Controls.Add(this.btn_DisplayProduct);
            this.panel_StorekeeperSideBoard.Controls.Add(this.guna2ImageButton1);
            this.panel_StorekeeperSideBoard.Controls.Add(this.guna2Separator2);
            this.panel_StorekeeperSideBoard.Controls.Add(this.guna2Separator1);
            this.panel_StorekeeperSideBoard.FillColor = System.Drawing.Color.HotPink;
            this.panel_StorekeeperSideBoard.FillColor2 = System.Drawing.Color.Red;
            this.panel_StorekeeperSideBoard.FillColor3 = System.Drawing.Color.LightCyan;
            this.panel_StorekeeperSideBoard.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_StorekeeperSideBoard.Location = new System.Drawing.Point(-19, -1);
            this.panel_StorekeeperSideBoard.Name = "panel_StorekeeperSideBoard";
            this.panel_StorekeeperSideBoard.ShadowDecoration.Depth = 10;
            this.panel_StorekeeperSideBoard.ShadowDecoration.Enabled = true;
            this.panel_StorekeeperSideBoard.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_StorekeeperSideBoard.Size = new System.Drawing.Size(258, 696);
            this.panel_StorekeeperSideBoard.TabIndex = 1;
            // 
            // guna2Button5
            // 
            this.guna2Button5.Animated = true;
            this.guna2Button5.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button5.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button5.BorderRadius = 20;
            this.guna2Button5.BorderThickness = 2;
            this.guna2Button5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button5.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button5.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button5.ForeColor = System.Drawing.Color.White;
            this.guna2Button5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button5.IndicateFocus = true;
            this.guna2Button5.Location = new System.Drawing.Point(28, 371);
            this.guna2Button5.Name = "guna2Button5";
            this.guna2Button5.ShadowDecoration.Depth = 1;
            this.guna2Button5.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button5.Size = new System.Drawing.Size(219, 55);
            this.guna2Button5.TabIndex = 2;
            this.guna2Button5.Text = "guna2Button1";
            this.guna2Button5.UseTransparentBackground = true;
            // 
            // guna2Button4
            // 
            this.guna2Button4.Animated = true;
            this.guna2Button4.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button4.BorderColor = System.Drawing.Color.Transparent;
            this.guna2Button4.BorderRadius = 20;
            this.guna2Button4.BorderThickness = 2;
            this.guna2Button4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button4.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.guna2Button4.IndicateFocus = true;
            this.guna2Button4.Location = new System.Drawing.Point(28, 310);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.ShadowDecoration.Depth = 1;
            this.guna2Button4.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2Button4.Size = new System.Drawing.Size(219, 55);
            this.guna2Button4.TabIndex = 2;
            this.guna2Button4.Text = "guna2Button1";
            this.guna2Button4.UseTransparentBackground = true;
            // 
            // btn_ProductManagement
            // 
            this.btn_ProductManagement.Animated = true;
            this.btn_ProductManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_ProductManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ProductManagement.BorderRadius = 20;
            this.btn_ProductManagement.BorderThickness = 2;
            this.btn_ProductManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ProductManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ProductManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ProductManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ProductManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_ProductManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ProductManagement.ForeColor = System.Drawing.Color.White;
            this.btn_ProductManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ProductManagement.IndicateFocus = true;
            this.btn_ProductManagement.Location = new System.Drawing.Point(28, 249);
            this.btn_ProductManagement.Name = "btn_ProductManagement";
            this.btn_ProductManagement.ShadowDecoration.Depth = 1;
            this.btn_ProductManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ProductManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_ProductManagement.TabIndex = 2;
            this.btn_ProductManagement.Text = "Quản lý hàng hóa";
            this.btn_ProductManagement.UseTransparentBackground = true;
            this.btn_ProductManagement.Click += new System.EventHandler(this.btn_ProductManagement_Click);
            // 
            // btn_ImportProduct
            // 
            this.btn_ImportProduct.Animated = true;
            this.btn_ImportProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_ImportProduct.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ImportProduct.BorderRadius = 20;
            this.btn_ImportProduct.BorderThickness = 2;
            this.btn_ImportProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ImportProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ImportProduct.FillColor = System.Drawing.Color.Transparent;
            this.btn_ImportProduct.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportProduct.ForeColor = System.Drawing.Color.White;
            this.btn_ImportProduct.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ImportProduct.IndicateFocus = true;
            this.btn_ImportProduct.Location = new System.Drawing.Point(28, 188);
            this.btn_ImportProduct.Name = "btn_ImportProduct";
            this.btn_ImportProduct.ShadowDecoration.Depth = 1;
            this.btn_ImportProduct.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ImportProduct.Size = new System.Drawing.Size(219, 55);
            this.btn_ImportProduct.TabIndex = 2;
            this.btn_ImportProduct.Text = "Nhập hàng hóa";
            this.btn_ImportProduct.UseTransparentBackground = true;
            this.btn_ImportProduct.Click += new System.EventHandler(this.btn_ImportProduct_Click);
            // 
            // btn_DisplayProduct
            // 
            this.btn_DisplayProduct.Animated = true;
            this.btn_DisplayProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_DisplayProduct.BorderColor = System.Drawing.Color.Transparent;
            this.btn_DisplayProduct.BorderRadius = 20;
            this.btn_DisplayProduct.BorderThickness = 2;
            this.btn_DisplayProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DisplayProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DisplayProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DisplayProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DisplayProduct.FillColor = System.Drawing.Color.Transparent;
            this.btn_DisplayProduct.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DisplayProduct.ForeColor = System.Drawing.Color.White;
            this.btn_DisplayProduct.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_DisplayProduct.IndicateFocus = true;
            this.btn_DisplayProduct.Location = new System.Drawing.Point(28, 127);
            this.btn_DisplayProduct.Name = "btn_DisplayProduct";
            this.btn_DisplayProduct.ShadowDecoration.Depth = 1;
            this.btn_DisplayProduct.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_DisplayProduct.Size = new System.Drawing.Size(219, 55);
            this.btn_DisplayProduct.TabIndex = 2;
            this.btn_DisplayProduct.Text = "Hiển thị hàng hóa";
            this.btn_DisplayProduct.UseTransparentBackground = true;
            this.btn_DisplayProduct.Click += new System.EventHandler(this.btn_DisplayProduct_Click);
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator2.Location = new System.Drawing.Point(29, 556);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator2.TabIndex = 4;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator1.Location = new System.Drawing.Point(31, 113);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator1.TabIndex = 0;
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.panel_Main.FillColor = System.Drawing.Color.White;
            this.panel_Main.Location = new System.Drawing.Point(244, -1);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Radius = 5;
            this.panel_Main.ShadowColor = System.Drawing.Color.Black;
            this.panel_Main.ShadowDepth = 95;
            this.panel_Main.ShadowShift = 8;
            this.panel_Main.Size = new System.Drawing.Size(1139, 696);
            this.panel_Main.TabIndex = 4;
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl1.TargetControl = this.panel_StorekeeperSideBoard;
            this.guna2DragControl1.TransparentWhileDrag = false;
            // 
            // btn_Exit
            // 
            this.btn_Exit.Animated = true;
            this.btn_Exit.BackColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderRadius = 20;
            this.btn_Exit.BorderThickness = 2;
            this.btn_Exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Exit.FillColor = System.Drawing.Color.Transparent;
            this.btn_Exit.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Exit.ForeColor = System.Drawing.Color.White;
            this.btn_Exit.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Exit.Image = global::MiniMarketManagement.Properties.Resources.exit__1_;
            this.btn_Exit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Exit.ImageOffset = new System.Drawing.Point(6, 0);
            this.btn_Exit.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Exit.IndicateFocus = true;
            this.btn_Exit.Location = new System.Drawing.Point(28, 633);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.ShadowDecoration.Depth = 1;
            this.btn_Exit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Exit.Size = new System.Drawing.Size(219, 55);
            this.btn_Exit.TabIndex = 2;
            this.btn_Exit.Text = "Thoát";
            this.btn_Exit.UseTransparentBackground = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_Zoom
            // 
            this.btn_Zoom.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btn_Zoom.Animated = true;
            this.btn_Zoom.BackColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.BorderRadius = 20;
            this.btn_Zoom.BorderThickness = 2;
            this.btn_Zoom.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Zoom.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Zoom.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Zoom.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Zoom.FillColor = System.Drawing.Color.Transparent;
            this.btn_Zoom.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Zoom.ForeColor = System.Drawing.Color.White;
            this.btn_Zoom.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Zoom.HoverState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Zoom.Image = ((System.Drawing.Image)(resources.GetObject("btn_Zoom.Image")));
            this.btn_Zoom.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Zoom.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Zoom.ImageSize = new System.Drawing.Size(31, 31);
            this.btn_Zoom.IndicateFocus = true;
            this.btn_Zoom.Location = new System.Drawing.Point(28, 572);
            this.btn_Zoom.Name = "btn_Zoom";
            this.btn_Zoom.ShadowDecoration.Depth = 1;
            this.btn_Zoom.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Zoom.Size = new System.Drawing.Size(219, 55);
            this.btn_Zoom.TabIndex = 2;
            this.btn_Zoom.Text = "Thu Nhỏ";
            this.btn_Zoom.UseTransparentBackground = true;
            // 
            // guna2ImageButton1
            // 
            this.guna2ImageButton1.AnimatedGIF = true;
            this.guna2ImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ImageButton1.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton1.HoverState.ImageSize = new System.Drawing.Size(100, 100);
            this.guna2ImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("guna2ImageButton1.Image")));
            this.guna2ImageButton1.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton1.ImageRotate = 0F;
            this.guna2ImageButton1.ImageSize = new System.Drawing.Size(90, 90);
            this.guna2ImageButton1.Location = new System.Drawing.Point(82, 3);
            this.guna2ImageButton1.Name = "guna2ImageButton1";
            this.guna2ImageButton1.PressedState.ImageSize = new System.Drawing.Size(95, 95);
            this.guna2ImageButton1.Size = new System.Drawing.Size(111, 118);
            this.guna2ImageButton1.TabIndex = 5;
            this.guna2ImageButton1.UseTransparentBackground = true;
            // 
            // frm_WarehouseManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 695);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.panel_StorekeeperSideBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_WarehouseManagement";
            this.Text = "WarehouseManagement";
            this.Load += new System.EventHandler(this.frm_WarehouseManagement_Load);
            this.panel_StorekeeperSideBoard.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_StorekeeperSideBoard;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_Main;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton1;
        private Guna.UI2.WinForms.Guna2Button guna2Button5;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2Button btn_ProductManagement;
        private Guna.UI2.WinForms.Guna2Button btn_ImportProduct;
        private Guna.UI2.WinForms.Guna2Button btn_DisplayProduct;
        private Guna.UI2.WinForms.Guna2Button btn_Zoom;
        private Guna.UI2.WinForms.Guna2Button btn_Exit;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
    }
}