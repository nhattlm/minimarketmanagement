﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Storekeeper;
namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_DisplayProduct : Form
    {
        public frm_DisplayProduct()
        {
            InitializeComponent();
        }

        private void frm_DisplayProduct_Load(object sender, EventArgs e)
        {
            BLL_DisplayProduct bLL_DisplayProduct = new BLL_DisplayProduct();
            bLL_DisplayProduct.FormDisplayProduct(dgv_ListProduct);
        }
        private void dgv_ListProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;
            if(row < 0)
            {
                return;
            }
            else
            {
                Image image;
                string ProductID;
                string ProductName;
                BLL_DisplayProduct bLL_DisplayProduct = new BLL_DisplayProduct();
                bLL_DisplayProduct.FillPicbox(row, dgv_ListProduct, out image,out ProductID,out ProductName);
                txt_ProductID.Text = ProductID;
                txt_ProductName.Text = ProductName;
                pb_ProductImage.Image = image;
            }
        }

        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            BLL_DisplayProduct bLL_DisplayProduct = new BLL_DisplayProduct();
            var txt = txt_Search.Text;
            bLL_DisplayProduct.SearchProduct(txt, dgv_ListProduct);
        }

        private void dgv_ListProduct_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_DisplayProduct bLL_DisplayProduct = new BLL_DisplayProduct();
            dgv_ListProduct.ClearSelection();
            string ID;
            string Name;
            Image image;
            bLL_DisplayProduct.ClearInputFields(out image, out ID, out Name);
            pb_ProductImage.Image = image;
            txt_ProductName.Text = Name;
            txt_ProductID.Text = ID;
        }
    }
}
