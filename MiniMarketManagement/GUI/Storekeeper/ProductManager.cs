﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Storekeeper;
using MiniMarketManagement.DAL.Cashier;

namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_ProductManager : Form
    {
        BLL_ProductManager bll_ProductManager = new BLL_ProductManager();
        public frm_ProductManager()
        {
            InitializeComponent();
        }

        private bool checkclick = true;

        private void frm_ProductManager_Load(object sender, EventArgs e)
        {
            bll_ProductManager.LoadDgvProduct(dgv_ListProduct);
        }

        private void dgv_ListProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Image image;
            bll_ProductManager.LoadCellClickDgvProduct(sender,e,dgv_ListProduct, txt_ProductID, txt_ProductName, txt_ProductTypeName
                , txt_CalculationUnitName, txt_Quantity, txt_SupplierName, txt_SellPrice, txt_Discount, out image);
            pic_Product.Image = image;
            txt_Search.Text = "";
        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {
            bll_ProductManager.FindProduct(dgv_ListProduct,txt_Search);
            txt_ProductID.Text = "";
            txt_ProductName.Text = "";
            txt_ProductTypeName.Text = "";
            txt_Quantity.Text = "";
            txt_SupplierName.Text = "";
            txt_CalculationUnitName.Text = "";
            txt_SellPrice.Text = "";
            txt_Discount.Text = "";
        }

        private void btn_UpdatePriceAndDiscount_Click(object sender, EventArgs e)
        {            
            bool checkError = bll_ProductManager.btn_Update_SellPrice_And_Discount(txt_ProductID, txt_SellPrice, txt_Discount); ;
            if (checkError == false)
            {
                return;
            }
            else
            {
                bll_ProductManager.LoadDgvProduct(dgv_ListProduct);
                txt_ProductID.Text = "";
                txt_ProductName.Text = "";
                txt_ProductTypeName.Text = "";
                txt_Quantity.Text = "";
                txt_SupplierName.Text = "";
                txt_CalculationUnitName.Text = "";
                txt_SellPrice.Text = "";
                txt_Search.Text = "";
                txt_Discount.Text = "";
            }
        }

        private void btn_DeleteProduct_Click(object sender, EventArgs e)
        {
            bool checkError = bll_ProductManager.btn_Delete_Product(txt_ProductID);
            if (checkError == false)
            {
                return;
            }
            else
            {
                bll_ProductManager.LoadDgvProduct(dgv_ListProduct);
                txt_ProductID.Text = "";
                txt_ProductName.Text = "";
                txt_ProductTypeName.Text = "";
                txt_Quantity.Text = "";
                txt_SupplierName.Text = "";
                txt_CalculationUnitName.Text = "";
                txt_SellPrice.Text = "";
                txt_Search.Text = "";
                txt_Discount.Text = "";
            }
        }
    }
}
