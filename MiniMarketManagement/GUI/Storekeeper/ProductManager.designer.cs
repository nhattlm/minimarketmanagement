﻿namespace MiniMarketManagement.GUI.Storekeeper
{
    partial class frm_ProductManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.dgv_ListProduct = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgv_ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_ProductTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_SellPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CalculationUnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_Discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pic_Product = new Guna.UI2.WinForms.Guna2PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_SupplierName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Quantity = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CalculationUnitName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Discount = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_SellPrice = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductTypeName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_UpdatePriceAndDiscount = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_DeleteProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListProduct)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(602, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(371, 41);
            this.label6.TabIndex = 9;
            this.label6.Text = "Danh sách sản phẩm";
            // 
            // dgv_ListProduct
            // 
            this.dgv_ListProduct.AllowUserToAddRows = false;
            this.dgv_ListProduct.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_ListProduct.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListProduct.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv_ListProduct.ColumnHeadersHeight = 18;
            this.dgv_ListProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_ProductID,
            this.dgv_ProductName,
            this.dgv_ProductTypeName,
            this.dgv_SellPrice,
            this.dgv_CalculationUnitName,
            this.dgv_ProductQuantity,
            this.dgv_Supplier,
            this.dgv_Discount});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListProduct.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_ListProduct.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.Location = new System.Drawing.Point(407, 53);
            this.dgv_ListProduct.Name = "dgv_ListProduct";
            this.dgv_ListProduct.ReadOnly = true;
            this.dgv_ListProduct.RowHeadersVisible = false;
            this.dgv_ListProduct.RowHeadersWidth = 51;
            this.dgv_ListProduct.RowTemplate.Height = 24;
            this.dgv_ListProduct.Size = new System.Drawing.Size(715, 570);
            this.dgv_ListProduct.TabIndex = 16;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListProduct.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListProduct.ThemeStyle.ReadOnly = true;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListProduct_CellClick);
            // 
            // dgv_ProductID
            // 
            this.dgv_ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_ProductID.FillWeight = 115F;
            this.dgv_ProductID.HeaderText = "Mã sản phẩm";
            this.dgv_ProductID.MinimumWidth = 6;
            this.dgv_ProductID.Name = "dgv_ProductID";
            this.dgv_ProductID.ReadOnly = true;
            // 
            // dgv_ProductName
            // 
            this.dgv_ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_ProductName.FillWeight = 120F;
            this.dgv_ProductName.HeaderText = "Tên sản phẩm";
            this.dgv_ProductName.MinimumWidth = 6;
            this.dgv_ProductName.Name = "dgv_ProductName";
            this.dgv_ProductName.ReadOnly = true;
            // 
            // dgv_ProductTypeName
            // 
            this.dgv_ProductTypeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_ProductTypeName.FillWeight = 120F;
            this.dgv_ProductTypeName.HeaderText = "Loại sản phẩm";
            this.dgv_ProductTypeName.MinimumWidth = 6;
            this.dgv_ProductTypeName.Name = "dgv_ProductTypeName";
            this.dgv_ProductTypeName.ReadOnly = true;
            // 
            // dgv_SellPrice
            // 
            this.dgv_SellPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_SellPrice.FillWeight = 75F;
            this.dgv_SellPrice.HeaderText = "Giá bán";
            this.dgv_SellPrice.MinimumWidth = 6;
            this.dgv_SellPrice.Name = "dgv_SellPrice";
            this.dgv_SellPrice.ReadOnly = true;
            // 
            // dgv_CalculationUnitName
            // 
            this.dgv_CalculationUnitName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CalculationUnitName.FillWeight = 70F;
            this.dgv_CalculationUnitName.HeaderText = "ĐVT";
            this.dgv_CalculationUnitName.MinimumWidth = 6;
            this.dgv_CalculationUnitName.Name = "dgv_CalculationUnitName";
            this.dgv_CalculationUnitName.ReadOnly = true;
            // 
            // dgv_ProductQuantity
            // 
            this.dgv_ProductQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_ProductQuantity.FillWeight = 80F;
            this.dgv_ProductQuantity.HeaderText = "Số lượng";
            this.dgv_ProductQuantity.MinimumWidth = 6;
            this.dgv_ProductQuantity.Name = "dgv_ProductQuantity";
            this.dgv_ProductQuantity.ReadOnly = true;
            // 
            // dgv_Supplier
            // 
            this.dgv_Supplier.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_Supplier.FillWeight = 115F;
            this.dgv_Supplier.HeaderText = "Nhà cung cấp";
            this.dgv_Supplier.MinimumWidth = 6;
            this.dgv_Supplier.Name = "dgv_Supplier";
            this.dgv_Supplier.ReadOnly = true;
            // 
            // dgv_Discount
            // 
            this.dgv_Discount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_Discount.FillWeight = 110F;
            this.dgv_Discount.HeaderText = "Giảm giá (%)";
            this.dgv_Discount.MinimumWidth = 6;
            this.dgv_Discount.Name = "dgv_Discount";
            this.dgv_Discount.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pic_Product);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_SupplierName);
            this.groupBox1.Controls.Add(this.txt_Quantity);
            this.groupBox1.Controls.Add(this.txt_CalculationUnitName);
            this.groupBox1.Controls.Add(this.txt_Discount);
            this.groupBox1.Controls.Add(this.txt_SellPrice);
            this.groupBox1.Controls.Add(this.txt_ProductTypeName);
            this.groupBox1.Controls.Add(this.txt_ProductName);
            this.groupBox1.Controls.Add(this.txt_ProductID);
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(389, 667);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin sản phẩm";
            // 
            // pic_Product
            // 
            this.pic_Product.BorderRadius = 5;
            this.pic_Product.Image = global::MiniMarketManagement.Properties.Resources.image__2_1;
            this.pic_Product.ImageRotate = 0F;
            this.pic_Product.Location = new System.Drawing.Point(178, 493);
            this.pic_Product.Name = "pic_Product";
            this.pic_Product.Size = new System.Drawing.Size(198, 168);
            this.pic_Product.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_Product.TabIndex = 6;
            this.pic_Product.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 345);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 24);
            this.label8.TabIndex = 5;
            this.label8.Text = "Nhà cung cấp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 295);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 24);
            this.label7.TabIndex = 5;
            this.label7.Text = "Số lượng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 24);
            this.label5.TabIndex = 5;
            this.label5.Text = "Đơn vị tính";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 446);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 24);
            this.label10.TabIndex = 5;
            this.label10.Text = "Giảm giá";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 395);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "Giá bán";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(155, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "Loại sản phẩm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 24);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tên sản phẩm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 24);
            this.label9.TabIndex = 5;
            this.label9.Text = "Tìm kiếm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mã sản phẩm";
            // 
            // txt_SupplierName
            // 
            this.txt_SupplierName.Animated = true;
            this.txt_SupplierName.BackColor = System.Drawing.Color.Transparent;
            this.txt_SupplierName.BorderColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.BorderRadius = 15;
            this.txt_SupplierName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SupplierName.DefaultText = "";
            this.txt_SupplierName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SupplierName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SupplierName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierName.Enabled = false;
            this.txt_SupplierName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SupplierName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_SupplierName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.IconLeft = global::MiniMarketManagement.Properties.Resources.agreement;
            this.txt_SupplierName.Location = new System.Drawing.Point(178, 345);
            this.txt_SupplierName.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_SupplierName.Name = "txt_SupplierName";
            this.txt_SupplierName.PasswordChar = '\0';
            this.txt_SupplierName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SupplierName.PlaceholderText = "Nhà cung cấp";
            this.txt_SupplierName.SelectedText = "";
            this.txt_SupplierName.ShadowDecoration.BorderRadius = 15;
            this.txt_SupplierName.ShadowDecoration.Enabled = true;
            this.txt_SupplierName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SupplierName.Size = new System.Drawing.Size(198, 35);
            this.txt_SupplierName.TabIndex = 4;
            // 
            // txt_Quantity
            // 
            this.txt_Quantity.Animated = true;
            this.txt_Quantity.BackColor = System.Drawing.Color.Transparent;
            this.txt_Quantity.BorderColor = System.Drawing.Color.Silver;
            this.txt_Quantity.BorderRadius = 15;
            this.txt_Quantity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Quantity.DefaultText = "";
            this.txt_Quantity.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Quantity.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Quantity.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Quantity.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Quantity.Enabled = false;
            this.txt_Quantity.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Quantity.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Quantity.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Quantity.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Quantity.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_Quantity.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Quantity.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Quantity.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Quantity.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Quantity.IconLeft = global::MiniMarketManagement.Properties.Resources.how_much1;
            this.txt_Quantity.Location = new System.Drawing.Point(178, 295);
            this.txt_Quantity.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_Quantity.Name = "txt_Quantity";
            this.txt_Quantity.PasswordChar = '\0';
            this.txt_Quantity.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Quantity.PlaceholderText = "Số lượng";
            this.txt_Quantity.SelectedText = "";
            this.txt_Quantity.ShadowDecoration.BorderRadius = 15;
            this.txt_Quantity.ShadowDecoration.Enabled = true;
            this.txt_Quantity.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Quantity.Size = new System.Drawing.Size(198, 35);
            this.txt_Quantity.TabIndex = 4;
            // 
            // txt_CalculationUnitName
            // 
            this.txt_CalculationUnitName.Animated = true;
            this.txt_CalculationUnitName.BackColor = System.Drawing.Color.Transparent;
            this.txt_CalculationUnitName.BorderColor = System.Drawing.Color.Silver;
            this.txt_CalculationUnitName.BorderRadius = 15;
            this.txt_CalculationUnitName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CalculationUnitName.DefaultText = "";
            this.txt_CalculationUnitName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CalculationUnitName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CalculationUnitName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CalculationUnitName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CalculationUnitName.Enabled = false;
            this.txt_CalculationUnitName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CalculationUnitName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CalculationUnitName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CalculationUnitName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CalculationUnitName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_CalculationUnitName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CalculationUnitName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CalculationUnitName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CalculationUnitName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CalculationUnitName.IconLeft = global::MiniMarketManagement.Properties.Resources.options;
            this.txt_CalculationUnitName.Location = new System.Drawing.Point(178, 245);
            this.txt_CalculationUnitName.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_CalculationUnitName.Name = "txt_CalculationUnitName";
            this.txt_CalculationUnitName.PasswordChar = '\0';
            this.txt_CalculationUnitName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CalculationUnitName.PlaceholderText = "Đơn vị tính";
            this.txt_CalculationUnitName.SelectedText = "";
            this.txt_CalculationUnitName.ShadowDecoration.BorderRadius = 15;
            this.txt_CalculationUnitName.ShadowDecoration.Enabled = true;
            this.txt_CalculationUnitName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CalculationUnitName.Size = new System.Drawing.Size(198, 35);
            this.txt_CalculationUnitName.TabIndex = 4;
            // 
            // txt_Discount
            // 
            this.txt_Discount.Animated = true;
            this.txt_Discount.BackColor = System.Drawing.Color.Transparent;
            this.txt_Discount.BorderColor = System.Drawing.Color.Silver;
            this.txt_Discount.BorderRadius = 15;
            this.txt_Discount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Discount.DefaultText = "";
            this.txt_Discount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Discount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Discount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Discount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Discount.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Discount.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Discount.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Discount.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Discount.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_Discount.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Discount.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Discount.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Discount.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Discount.IconLeft = global::MiniMarketManagement.Properties.Resources.Discount;
            this.txt_Discount.Location = new System.Drawing.Point(178, 446);
            this.txt_Discount.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_Discount.Name = "txt_Discount";
            this.txt_Discount.PasswordChar = '\0';
            this.txt_Discount.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Discount.PlaceholderText = "Giảm giá";
            this.txt_Discount.SelectedText = "";
            this.txt_Discount.ShadowDecoration.BorderRadius = 15;
            this.txt_Discount.ShadowDecoration.Enabled = true;
            this.txt_Discount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Discount.Size = new System.Drawing.Size(198, 35);
            this.txt_Discount.TabIndex = 4;
            // 
            // txt_SellPrice
            // 
            this.txt_SellPrice.Animated = true;
            this.txt_SellPrice.BackColor = System.Drawing.Color.Transparent;
            this.txt_SellPrice.BorderColor = System.Drawing.Color.Silver;
            this.txt_SellPrice.BorderRadius = 15;
            this.txt_SellPrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SellPrice.DefaultText = "";
            this.txt_SellPrice.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SellPrice.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SellPrice.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SellPrice.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SellPrice.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SellPrice.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SellPrice.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SellPrice.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SellPrice.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_SellPrice.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SellPrice.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SellPrice.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SellPrice.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SellPrice.IconLeft = global::MiniMarketManagement.Properties.Resources.price_tag;
            this.txt_SellPrice.Location = new System.Drawing.Point(178, 395);
            this.txt_SellPrice.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_SellPrice.Name = "txt_SellPrice";
            this.txt_SellPrice.PasswordChar = '\0';
            this.txt_SellPrice.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SellPrice.PlaceholderText = "Giá bán";
            this.txt_SellPrice.SelectedText = "";
            this.txt_SellPrice.ShadowDecoration.BorderRadius = 15;
            this.txt_SellPrice.ShadowDecoration.Enabled = true;
            this.txt_SellPrice.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SellPrice.Size = new System.Drawing.Size(198, 35);
            this.txt_SellPrice.TabIndex = 4;
            // 
            // txt_ProductTypeName
            // 
            this.txt_ProductTypeName.Animated = true;
            this.txt_ProductTypeName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductTypeName.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductTypeName.BorderRadius = 15;
            this.txt_ProductTypeName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductTypeName.DefaultText = "";
            this.txt_ProductTypeName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductTypeName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductTypeName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductTypeName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductTypeName.Enabled = false;
            this.txt_ProductTypeName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductTypeName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductTypeName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductTypeName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductTypeName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_ProductTypeName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductTypeName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductTypeName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductTypeName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductTypeName.IconLeft = global::MiniMarketManagement.Properties.Resources.text__1_;
            this.txt_ProductTypeName.Location = new System.Drawing.Point(178, 195);
            this.txt_ProductTypeName.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_ProductTypeName.Name = "txt_ProductTypeName";
            this.txt_ProductTypeName.PasswordChar = '\0';
            this.txt_ProductTypeName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductTypeName.PlaceholderText = "Loại sản phẩm";
            this.txt_ProductTypeName.SelectedText = "";
            this.txt_ProductTypeName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductTypeName.ShadowDecoration.Enabled = true;
            this.txt_ProductTypeName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductTypeName.Size = new System.Drawing.Size(198, 35);
            this.txt_ProductTypeName.TabIndex = 4;
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Animated = true;
            this.txt_ProductName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductName.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductName.BorderRadius = 15;
            this.txt_ProductName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductName.DefaultText = "";
            this.txt_ProductName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.Enabled = false;
            this.txt_ProductName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_ProductName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.IconLeft = global::MiniMarketManagement.Properties.Resources.features;
            this.txt_ProductName.Location = new System.Drawing.Point(178, 145);
            this.txt_ProductName.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PasswordChar = '\0';
            this.txt_ProductName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.PlaceholderText = "Tên sản phẩm";
            this.txt_ProductName.SelectedText = "";
            this.txt_ProductName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductName.ShadowDecoration.Enabled = true;
            this.txt_ProductName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductName.Size = new System.Drawing.Size(198, 35);
            this.txt_ProductName.TabIndex = 4;
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Animated = true;
            this.txt_ProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductID.BorderRadius = 15;
            this.txt_ProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductID.DefaultText = "";
            this.txt_ProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.Enabled = false;
            this.txt_ProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_ProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_;
            this.txt_ProductID.Location = new System.Drawing.Point(178, 96);
            this.txt_ProductID.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PasswordChar = '\0';
            this.txt_ProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductID.PlaceholderText = "Tìm kiếm";
            this.txt_ProductID.SelectedText = "";
            this.txt_ProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductID.ShadowDecoration.Enabled = true;
            this.txt_ProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductID.Size = new System.Drawing.Size(198, 35);
            this.txt_ProductID.TabIndex = 4;
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.Silver;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_Search.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_Search.Location = new System.Drawing.Point(178, 45);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Tìm kiếm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(198, 35);
            this.txt_Search.TabIndex = 4;
            this.txt_Search.TextChanged += new System.EventHandler(this.guna2TextBox1_TextChanged);
            // 
            // btn_UpdatePriceAndDiscount
            // 
            this.btn_UpdatePriceAndDiscount.BackColor = System.Drawing.Color.Transparent;
            this.btn_UpdatePriceAndDiscount.BorderRadius = 10;
            this.btn_UpdatePriceAndDiscount.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_UpdatePriceAndDiscount.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_UpdatePriceAndDiscount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_UpdatePriceAndDiscount.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_UpdatePriceAndDiscount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_UpdatePriceAndDiscount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_UpdatePriceAndDiscount.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_UpdatePriceAndDiscount.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btn_UpdatePriceAndDiscount.ForeColor = System.Drawing.Color.White;
            this.btn_UpdatePriceAndDiscount.Image = global::MiniMarketManagement.Properties.Resources.exchange_rate;
            this.btn_UpdatePriceAndDiscount.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_UpdatePriceAndDiscount.Location = new System.Drawing.Point(464, 629);
            this.btn_UpdatePriceAndDiscount.Name = "btn_UpdatePriceAndDiscount";
            this.btn_UpdatePriceAndDiscount.ShadowDecoration.BorderRadius = 10;
            this.btn_UpdatePriceAndDiscount.ShadowDecoration.Enabled = true;
            this.btn_UpdatePriceAndDiscount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_UpdatePriceAndDiscount.Size = new System.Drawing.Size(231, 50);
            this.btn_UpdatePriceAndDiscount.TabIndex = 8;
            this.btn_UpdatePriceAndDiscount.Text = "Sửa giá tiền/Giảm giá";
            this.btn_UpdatePriceAndDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_UpdatePriceAndDiscount.Click += new System.EventHandler(this.btn_UpdatePriceAndDiscount_Click);
            // 
            // btn_DeleteProduct
            // 
            this.btn_DeleteProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_DeleteProduct.BorderRadius = 10;
            this.btn_DeleteProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DeleteProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_DeleteProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_DeleteProduct.Font = new System.Drawing.Font("Tahoma", 11F);
            this.btn_DeleteProduct.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteProduct.Image = global::MiniMarketManagement.Properties.Resources.delete_product;
            this.btn_DeleteProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteProduct.Location = new System.Drawing.Point(752, 629);
            this.btn_DeleteProduct.Name = "btn_DeleteProduct";
            this.btn_DeleteProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_DeleteProduct.ShadowDecoration.Enabled = true;
            this.btn_DeleteProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_DeleteProduct.Size = new System.Drawing.Size(173, 50);
            this.btn_DeleteProduct.TabIndex = 8;
            this.btn_DeleteProduct.Text = "Xóa sản phẩm";
            this.btn_DeleteProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_DeleteProduct.Click += new System.EventHandler(this.btn_DeleteProduct_Click);
            // 
            // frm_ProductManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgv_ListProduct);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btn_UpdatePriceAndDiscount);
            this.Controls.Add(this.btn_DeleteProduct);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ProductManager";
            this.Text = "ProductManager";
            this.Load += new System.EventHandler(this.frm_ProductManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListProduct)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2GradientButton btn_DeleteProduct;
        private Guna.UI2.WinForms.Guna2GradientButton btn_UpdatePriceAndDiscount;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_Quantity;
        private Guna.UI2.WinForms.Guna2TextBox txt_CalculationUnitName;
        private Guna.UI2.WinForms.Guna2TextBox txt_SellPrice;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductTypeName;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductName;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2TextBox txt_SupplierName;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductID;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private Guna.UI2.WinForms.Guna2PictureBox pic_Product;
        private System.Windows.Forms.Label label10;
        private Guna.UI2.WinForms.Guna2TextBox txt_Discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_ProductTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_SellPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CalculationUnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Discount;
    }
}