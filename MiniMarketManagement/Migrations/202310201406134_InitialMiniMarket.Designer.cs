﻿// <auto-generated />
namespace MiniMarketManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class InitialMiniMarket : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialMiniMarket));
        
        string IMigrationMetadata.Id
        {
            get { return "202310201406134_InitialMiniMarket"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
