namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Statistic
    {
        public double PriceTotal { get; set; }

        [Key]
        [Column(Order = 0, TypeName = "date")]
        public DateTime DateEnd { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime DateStart { get; set; }

        public double ExpendsTotal { get; set; }

        public double ProfitTotal { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeID { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
