﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniMarketManagement.DAL.Entities;
namespace MiniMarketManagement.DAL.Storekeeper
{
    internal class DAL_ReturnProduct_Storekeeper
    {
        public List<DetailImportProductCard> GetListDetailImportProductCards()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailImportProductCards.ToList();
        }
        public Repository GetRepositoryByProductIDandSupplierID(string productID, string SupplierID)
        {
            MiniMarketDB miniMarketDB = new MiniMarketDB();
            return miniMarketDB.Repositories.SingleOrDefault(x => x.ProductID == productID && x.SupplierID == SupplierID);
        }
        public ImportProductCard GetImportProductCardByImportProductID(string IProductID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.ImportProductCards.SingleOrDefault(x => x.ImportProductID == IProductID);
        }
        public DetailImportProductCard GetDetailImportProductCardByIPCIDAndPID(string IPCID, string PID)
        {
            MiniMarketDB miniMarketDB = new MiniMarketDB();
            return miniMarketDB.DetailImportProductCards.SingleOrDefault(x => x.ProductID == PID && x.ImportProductID == IPCID);
        }
        public int GetLastIndexOfReturnProductCard()
        {
            MiniMarketDB context = new MiniMarketDB();
            bool check = !context.ReturnProductsCards.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = context.ReturnProductsCards
                    .Select(item => new
                    {
                        NumberPart = item.ReturnProductID.Substring(2)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
    }
}
